<?php
	class Member_model extends CI_Model
	{
		
		function  __construct() 
		{
			parent::__construct();
		}
		
		function getMember()
		{
			$res = $this->db->query("select * from member")->result();
			return $res;
		}
		function getMemberKode($kode)
		{
			$res = $this->db->query("SELECT * from member left join komplain on komplain.member_number = member.id_member left join komplainTanggapan on komplainTanggapan.id_komplain = komplain.id where member.kode_member like '$kode' ");
			return $res;
		}
		function getMemberId($id)
		{
			$res = $this->db->query("select * from member LEFT JOIN agama ON agama.id = member.idagama
LEFT JOIN status s ON s.id = member.idstatus
LEFT JOIN gender ON gender.id = member.idgender
LEFT JOIN pekerjaan ON pekerjaan.id = member.idpekerjaan where id_member= $id")->result();
			return $res;
		}
		function getAgama(){
			$res=$this->db->query("select * from agama")->result();
			return $res;
		}function getStatus(){
			$res=$this->db->query("select * from status")->result();
			return $res;
		}function getGender(){
			$res=$this->db->query("select * from gender")->result();
			return $res;
		}function getPekerjaan(){
			$res=$this->db->query("select * from pekerjaan")->result();
			return $res;
		}
		function getKaryawan(){
			$res = $this->db->query("select * from karyawan left join t_jabatan on t_jabatan.id_jabatan = karyawan.id_jabatan")->result();
			return $res;
		}function getKaryawans($id){
			$res = $this->db->query("select * from karyawan left join t_jabatan on t_jabatan.id_jabatan = karyawan.id_jabatan where karyawan.id = $id")->result();
			return $res;
		}function getKaryawanId($id){
			$qry ="select karyawan.*,time_work.senin,t_jabatan.nama_jabatan,t_jabatan.id_jabatan,time_work.minggu,time_work.selasa,time_work.rabu,time_work.kamis,time_work.jumat,time_work.sabtu,time_work.id_karyawan,time_work.jam_masuk,time_work.jam_keluar from karyawan left join t_jabatan on t_jabatan.id_jabatan = karyawan.id_jabatan left join time_work on time_work.id_karyawan = karyawan.id where karyawan.id=$id";
			$res = $this->db->query($qry)->result();
			return $res;
		}
		
		function getJabatan(){
			$res=$this->db->query("select * from t_jabatan")->result();
			return $res;
		}
		function getKategoriKomplain(){
			$res=$this->db->query("select * from t_kategori_komplain")->result();
			return $res;
		}
		function getTimework(){
			$res = $this->db->query("select karyawan.name,time_work.* from time_work left join karyawan on karyawan.id = time_work.id_karyawan")->result();
			return $res;
		}
		function getTimeworkId($id){
			$res = $this->db->query("select karyawan.name,time_work.* from time_work left join karyawan on karyawan.id = time_work.id_karyawan where time_work.id = $id")->result();
			return $res;
		}
		function getTimekaryawan(){
			$res= $this->db->query("select * from karyawan where id not in(select id_karyawan from time_work)")->result();
			return $res;
		}
		function getKomplainId($id){
			$res= $this->db->query("select 
				member.id_member,
				member.kode_member, 
				komplain.id as id_komplain,
				t_kategori_komplain.nm_kategori,
				member.nama,
				komplain.* 
				from 
				komplain 
				left join member on member.id_member = komplain.member_number 
				left join t_kategori_komplain on t_kategori_komplain.id_kategori = komplain.kategori_number 
				where komplain.id = $id");
			return $res;
		}
		function getKomplainIdMember($id){
			$res= $this->db->query("select member.kode_member, komplain.id as id_komplain,t_kategori_komplain.nm_kategori,member.nama,komplain.* from komplain left join member on member.id_member = komplain.member_number left join t_kategori_komplain on t_kategori_komplain.id_kategori = komplain.kategori_number where member.id_member = $id");
			return $res;
		}
		function getKomplainIdwithmember($id,$idmember){
			$res= $this->db->query("select member.kode_member, komplain.id as id_komplain,t_kategori_komplain.nm_kategori,member.nama,komplain.* from komplain left join member on member.id_member = komplain.member_number left join t_kategori_komplain on t_kategori_komplain.id_kategori = komplain.kategori_number where member.id_member = $id  and komplain.id = $idmember");
			return $res;
		}
		function getTanggapanId($id){
			// $res= $this->db->query("select * from komplainTanggapan left join karyawan on karyawan.id = komplainTanggapan.id_karyawan where komplainTanggapan.id_komplain = $id and komplainTanggapan.id_karyawan != 0");
			$res= $this->db->query(
				"select * from komplainTanggapan 
left join karyawan on karyawan.id = komplainTanggapan.id_karyawan 
left join member on member.id_member = komplainTanggapan.member_id
where komplainTanggapan.id_komplain = $id"
				);
			return $res;
		}function getTanggapanIdMember($id){
			$res= $this->db->query("select * from komplainTanggapan left join member on member.id_member = komplainTanggapan.member_id where komplainTanggapan.id_komplain = $id and komplainTanggapan.member_id != 0");
			return $res;
		}
		function getKomplainTanggapanId($id){
			$res= $this->db->query("select * from komplain left join member on member.id_member = komplain.member_number left join t_kategori_komplain on t_kategori_komplain.id_kategori = komplain.kategori_number left join komplainTanggapan on komplainTanggapan.id_komplain = komplain.id where member.id_member = $id");
			return $res;
		}
		function getKomplain(){
			$res= $this->db->query("select * from komplain left join member on member.id_member = komplain.member_number left join t_kategori_komplain on t_kategori_komplain.id_kategori = komplain.kategori_number ");
			return $res;
		}
		function getKomplainfind($idkategori,$tglawal,$tglakhir){
			$tgl = "";
			if(!empty($tglawal) || !empty($tglakhir)){
				$tgl=" and date(komplain.date_created ) between '$tglawal' and '$tglakhir'";
			}
			$isi=("select * from komplain left join member on member.id_member = komplain.member_number left join t_kategori_komplain on t_kategori_komplain.id_kategori = komplain.kategori_number where  kategori_number = $idkategori $tgl");
			$res= $this->db->query($isi);
			return $res;
		}
				function getPenanggungJawab(){
			$res = $this->db->query("select * from penanggung_jawab left join jabatan on t_jabatan.id_jabatan = penanggung_jawab.id_jabatan left join t_kategori_komplain.id_kategori = penanggung_jawab.id_kategori_komplain")->resul();
			return $res;
		}
		function getTotalKomplain($where=null){
			$qry = "SELECT *
				FROM t_kategori_komplain
				LEFT JOIN (
					SELECT count( komplain.kategori_number ) total, komplain . *
					FROM komplain
					$where
					GROUP BY komplain.kategori_number
				)as xy ON xy.kategori_number = t_kategori_komplain.id_kategori ";
			return $this->db->query($qry)->result();
		}

		
	}
