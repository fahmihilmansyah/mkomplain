<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {


	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 * 
	 * Hak_akses Ada 3:
	 * 1. SPV bisa lihat dan jawab komplain
	 * 2. Admin yang bisa ngatur semuanya
	 * 3. Manager hanya bisa liat data yang komplain
	 */
	function __construct(){
		parent::__construct();
        $this->load->library('session');
		$this->load->model("Member_model");
		$this->load->model("Login_model");
		
	}
    public function index(){
//        $newdata = array(
//                           'username'  => 'johndoe',
//                           'email'     => 'johndoe@some-site.com',
//                           'logged_in' => TRUE
//                       );
//
//        $this->session->set_userdata($newdata);
		if($this->session->userdata('logged_in')){
			redirect('admin');
		}
		$this->load->view('backend/loginView');
	}
	function doLogin(){
		$username = $this->input->post('username',true);
		$password = $this->input->post('password',true);
		$cek = $this->db->query("select login.id as login_id, karyawan.name, karyawan.id_jabatan,login.* from login left join karyawan on login.id_karyawan = karyawan.id where login.username = '".$username."' and login.password = '".md5($password)."' ");
		$ceks = $cek->result();
		if($cek->num_rows() > 0){
			$newdata = array(
                   'username'  => $username,
                   'name'  => $ceks[0]->name,
                   'id_karyawan'  => $ceks[0]->id_karyawan,
                   'jabatan'  => $ceks[0]->id_jabatan,
                   'hak_akses'  => $ceks[0]->hak_akses,
                   'id_login'  => $ceks[0]->login_id,
                   'logged_in' => TRUE
               );
			$this->session->set_userdata($newdata);

			echo "<script>alert('Welcome');
			window.location.href= '".base_url()."admin';
			</script>";
			//redirect('admin');
			
		}else{
			echo "<script>alert('maaf username dan password tidak ditemukan');
			window.location.href= '".base_url()."login';
			</script>";
		//redirect('login');
		
			
		}
	}
	function listLogin(){
		$data['isi'] = $this->Login_model->getListLogin();
		$data['content']="backend/loginList";
		$this->load->view("template/content",$data);
	}
	function doDestroy(){
		$this->session->sess_destroy();
		redirect('login');
	}
	function editLogin($id){
		$data['isi'] = $this->Login_model->getListLoginId($id);
		$data['karyawan'] = $this->Member_model->getKaryawan();
		$data['content']="backend/loginEdit";
		$this->load->view("template/content",$data);
	}
	function createLogin(){
		$data['karyawan'] = $this->Login_model->getListLoginL();
		$data['content']="backend/loginCreate";
		$this->load->view("template/content",$data);
	}
	function doEdit($id){
		$username=$this->input->post('username');
		$password=md5($this->input->post('password'));
		$hak_akses=$this->input->post('hak_akses');
		$data = array(
		'username' => $username,
		'password' => $password,
		'hak_akses' => $hak_akses,
			);
		$this->db->where('id',$id);
		$this->db->update("login",$data);
		redirect('login/listlogin');
	}
	function doCreate(){
		$id_karyawan=$this->input->post('id_karyawan');
		$username=$this->input->post('username');
		$password=md5($this->input->post('password'));
		$hak_akses=$this->input->post('hak_akses');
		$data = array(
		'id_karyawan' => $id_karyawan,
		'username' => $username,
		'password' => $password,
		'hak_akses' => $hak_akses,
			);
		$this->db->insert('login',$data);
		redirect('login/listlogin');
	}
	function doDelete($id){
		$this->db->where("id",$id);
		$this->db->delete('login');
		if($this->session->userdata('id_login') == $id):
			$this->doDestroy();
		else:
			redirect('login/listlogin');
		endif;
	}
}
