<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Member extends CI_Controller {


	function __construct(){
		parent::__construct();
		$this->load->model("Member_model");
	}
	public function index()
	{
		$data['isi'] = $this->Member_model->getKategoriKomplain();
		$data['content']="frontend/memberComplaint";
		$this->load->view('content',$data);
		/*$this->load->view('frontend/member',$data);*/
	}
	public function checkMember(){
		$kode_member = $this->input->post('kode_member');
		$data['result'] = false;
		if(!empty($kode_member) || $kode_member != null){
			$check = $this->Member_model->getMemberKode($kode_member);
			$checks = $check->result();
			if($check->num_rows() > 0){
				$data['result'] = true;
				$data['isi'] = array('nama' => $checks[0]->nama,'email' => $checks[0]->email,'idmember' => $checks[0]->id_member);
			}
		}
		echo json_encode($data);
	}
	public function doInsertKomplain(){
		$kdKat = $this->input->post('kdKat');
		$idmember = $this->input->post('idmember');
		if(empty($idmember)){
			redirect('');
		}else {
		$nmComp = $this->input->post('nmComp');
		$prio = $this->input->post('prio');
		$islogedin = $this->session->userdata('logged_in');
		$username = $this->session->userdata('username');
		$data = array('member_number' => $idmember,
			'kategori_number' => $kdKat,
			'isi_komplain' => $nmComp,
			'date_created' => date('Y-m-d H:i:s'),
			'prioritas' => isset($prio)?$prio:"Low Complaint",
			'created_by' => isset($islogedin)?$username:'SYSTEM',
			);
		
		$this->db->insert("komplain",$data);
		echo "<script>alert('terima kasih atas komplainnya, komplain anda akan kami tanggapi secepatnya');
			window.location.href= '".base_url()."';
			</script>";
		//redirect("member");
		}
	}
	public function checkKomplain(){
		$kode_member = $this->input->post('kode_member');
		$data['result'] = false;
		if(!empty($kode_member) || $kode_member != null){
			$check = $this->Member_model->getKomplainIdMember($kode_member);
			$checks = $check->result();
			if($check->num_rows() > 0){
				$data['result'] = true;
				foreach ($checks as $row) {
					$data['isi'][] = array(
						'nama' => $row->nama,
						'isi_komplain' => $row->isi_komplain,
						'id_komplain' => $row->id_komplain,
						'nm_kategori' => $row->nm_kategori,
						'date_created' => $row->date_created,
						'active' => $row->active,
						);
				}
			}
		}
		echo json_encode($data);
	}
	public function checkKomplainTanggapan(){
		$kode_member = $this->input->post('kode_member');
		$data['result'] = false;
		if(!empty($kode_member) || $kode_member != null){
			$check = $this->Member_model->getKomplainTanggapanId($kode_member);
			$checks = $check->result();
			if($check->num_rows() > 0){
				$data['result'] = true;
				foreach ($checks as $row) {
					$data['isi'][] = array(
						'nama' => $row->nama,
						'isi_komplain' => $row->isi_komplain,
						'nm_kategori' => $row->nm_kategori,
						'date_created' => $row->date_created,
						);
				}
			}
		}
		echo json_encode($data);
	}
	public function getComplaintDetail($idkomplain,$idmember){
		$data['isi']=$this->Member_model->getKomplainId($idkomplain)->result();
		$data['isi1']=$this->Member_model->getTanggapanId($idkomplain)->result();
		//var_dump($data['isi']);
		$data['content']="backend/memberAnsware";
		$this->load->view('content',$data);
	}
	public function doAnswareComplaint(){
		$member_id = $this->input->post('member_id');
		$id_komplain = $this->input->post('id_komplain');
		$isi_tanggapan = $this->input->post('isi_tanggapan');
		$data = array(
			'member_id' => $member_id,
			'id_komplain' => $id_komplain,
			'isi_tanggapan' => $isi_tanggapan,
			'date_created' => date("Y-m-d H:i:s"),
			);
		$this->db->insert("komplainTanggapan",$data);

		$data1 = array(
			'active' => 1
			);
		$this->db->where('id',$id_komplain);
		$this->db->update('komplain',$data1);
		redirect('member');
	}
}
