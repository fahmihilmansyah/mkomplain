<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Complaint extends CI_Controller {


	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct(){
		parent::__construct();
		$this->load->model("Member_model");
		if(!$this->session->userdata('logged_in')){
			redirect('login');
		}
	}
	public function index()
	{
		$this->listComplaint();
	}
	//komplain sudah
	public function Complaintsudah()
	{
		
		$data['isi']=$this->Member_model->getKomplain()->result();
		//var_dump($data['isi']);
		$data['content']="backend/complaintsudah";
		$this->load->view('template/content',$data);
	}
	//komplain sudah
	public function Complaintbelum()
	{
		
		$data['isi']=$this->Member_model->getKomplain()->result();
		//var_dump($data['isi']);
		$data['content']="backend/complaintbelum";
		$this->load->view('template/content',$data);
	}
	//komplain sudahbelum
	public function Complaintsudahblm()
	{
		
		$data['isi']=$this->Member_model->getKomplain()->result();
		//var_dump($data['isi']);
		$data['content']="backend/complaintsudahblm";
		$this->load->view('template/content',$data);
	}
	public function listComplaint()
	{
		
		$data['isi']=$this->Member_model->getKomplain()->result();
		//var_dump($data['isi']);
		$data['content']="backend/complaintList";
		$this->load->view('template/content',$data);
	}
	public function answareComplaint($id)
	{
		if($this->session->userdata('hak_akses')==3){
			redirect('complaint');
		}
		$data['isi']=$this->Member_model->getKomplainId($id)->result();
		$data['isi1']=$this->Member_model->getTanggapanId($id)->result();
		//var_dump($data['isi']);
		$data['content']="backend/complaintAnsware";
		$this->load->view('template/content',$data);
	}
	public function answareComplaintList($id)
	{
		$data['isi']=$this->Member_model->getKomplainId($id)->result();
		$data['isi1']=$this->Member_model->getTanggapanId($id)->result();
		
		//var_dump($data['isi']);
		$data['content']="backend/complaintAnswareList";
		$this->load->view('template/content',$data);
	}
	public function doAnswareComplaint(){
		$id_karyawan = $this->input->post('id_karyawan');
		$id_komplain = $this->input->post('id_komplain');
		$isi_tanggapan = $this->input->post('isi_tanggapan');
		$data = array(
			'id_karyawan' => $id_karyawan,
			'id_komplain' => $id_komplain,
			'isi_tanggapan' => $isi_tanggapan,
			'date_created' => date("Y-m-d H:i:s"),
			);
		$this->db->insert("komplainTanggapan",$data);

		$data1 = array(
			'active' => 2
			);
		$this->db->where('id',$id_komplain);
		$this->db->update('komplain',$data1);
		redirect('complaint');
	}
	function createComplaint(){
		$data['isi'] = $this->Member_model->getKategoriKomplain();
		$data['content']="backend/createComplaint";
		$this->load->view('template/content',$data);
	}
	function lapComplaint(){
		$where =' ';
		$tglawal='';
		$tglakhir='';
		if($_POST){
			$tglawal=$this->input->post('tglawal');
			$tglakhir=$this->input->post('tglakhir');
			$where .= " where date(date_created) between '$tglawal' and '$tglakhir' ";
		}
		$data['isi'] = $this->Member_model->getTotalKomplain($where);
		$data['tglawal'] = $tglawal;
		$data['tglakhir'] = $tglakhir;
		$data['content']="backend/lapKomplain";
		$this->load->view('template/content',$data);
	}
	function findcomplaint($idkategori=null, $tglawal=null,$tglakhir=null){
		$data['isi'] = $this->Member_model->getKomplainfind($idkategori, $tglawal,$tglakhir)->result();
		$data['content']="backend/complaintFind";
		$this->load->view('template/content',$data);
	}
}


