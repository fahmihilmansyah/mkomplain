<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model("Member_model");
		if(!$this->session->userdata('logged_in')  ){
			redirect('login');
		}
		if($this->session->userdata('hak_akses')!=2){
			redirect('complaint');
		}
	}
	public function index()
	{
		return redirect('komplain');
		$data['isi']=$this->Member_model->getMember();
		//var_dump($data['isi']);
		$data['content']="backend/memberList";/* View */
		$this->load->view('template/content',$data);
	}
	public function listMember()
	{
		
		$data['isi']=$this->Member_model->getMember();
		//var_dump($data['isi']);
		$data['content']="backend/memberList";/* View */
		$this->load->view('template/content',$data);
	}
	public function getMember($id){
		$data['qry'] = $this->Member_model->getMemberId($id);
		//echo "<pre>";
		//var_dump($data['qry']);
		$data['content']="backend/memberView";/* View */
		$this->load->view('template/content',$data);
	}
	public function editMember($id){
		$data['qry'] = $this->Member_model->getMemberId($id);
		$data['agama'] = $this->Member_model->getAgama();
		$data['status'] = $this->Member_model->getStatus();
		$data['gender'] = $this->Member_model->getGender();
		$data['pekerjaan'] = $this->Member_model->getPekerjaan();

		//echo "<pre>";
		//var_dump($data['pekerjaan']);
		$data['content']="backend/memberEdit";/* View */
		$this->load->view('template/content',$data);
	}
	public function createMember(){
		$data['agama'] = $this->Member_model->getAgama();
		$data['status'] = $this->Member_model->getStatus();
		$data['gender'] = $this->Member_model->getGender();
		$data['pekerjaan'] = $this->Member_model->getPekerjaan();

		//echo "<pre>";
		//var_dump($data['pekerjaan']);
		$data['content']="backend/memberCreate";/* View */
		$this->load->view('template/content',$data);
	}
	public function doEditMember($id){
	$kode_member =$this->input->post('kode_member');
	$nama =$this->input->post('nama');
	$no_ktp_sim =$this->input->post('no_ktp_sim');
	$alamat =$this->input->post('alamat');
	$email =$this->input->post('email');
	$lahir =$this->input->post('lahir');
	$ahli_waris =$this->input->post('ahli_waris');
	$agama =$this->input->post('agama');
	$no_tlp =$this->input->post('no_tlp');
	$status =$this->input->post('status');
	$gender =$this->input->post('gender');
	$pendidikan =$this->input->post('pendidikan');
	$tanggal_lahir =$this->input->post('tanggal_lahir');
	$pekerjaan =$this->input->post('pekerjaan');
	$data = array(
		'kode_member' => $kode_member,
		'nama' => $nama,
		'no_ktp_sim' => $no_ktp_sim,
		'alamat' => $alamat,
		'email' => $email,
		'lahir' => $lahir,
		'ahli_waris' => $ahli_waris,
		'idagama' => $agama,
		'no_tlp' => $no_tlp,
		'idstatus' => $status,
		'tanggal_lahir' => $tanggal_lahir,
		'idgender' => $gender,
		'pendidikan' => $pendidikan,
		'idpekerjaan' => $pekerjaan	
	);
	$this->db->where('id_member', $id);
	$this->db->update('member',$data);
	redirect('admin');
	}
	public function doInsertMember(){
	$kode_member =$this->input->post('kode_member');
	$nama =$this->input->post('nama');
	$no_ktp_sim =$this->input->post('no_ktp_sim');
	$alamat =$this->input->post('alamat');
	$email =$this->input->post('email');
	$lahir =$this->input->post('lahir');
	$ahli_waris =$this->input->post('ahli_waris');
	$agama =$this->input->post('agama');
	$no_tlp =$this->input->post('no_tlp');
	$status =$this->input->post('status');
	$gender =$this->input->post('gender');
	$tanggal_lahir =$this->input->post('tanggal_lahir');
	$pendidikan =$this->input->post('pendidikan');
	$pekerjaan =$this->input->post('pekerjaan');
	$data = array(
		'kode_member' => $kode_member,
		'nama' => $nama,
		'no_ktp_sim' => $no_ktp_sim,
		'alamat' => $alamat,
		'email' => $email,
		'lahir' => $lahir,
		'ahli_waris' => $ahli_waris,
		'idagama' => $agama,
		'no_tlp' => $no_tlp,
		'idstatus' => $status,
		'tanggal_lahir' => $tanggal_lahir,
		'idgender' => $gender,
		'pendidikan' => $pendidikan,
		'idpekerjaan' => $pekerjaan	
	);
	/*pengecekan jika ada member di db maka langsung di redirect tidak di simpan*/
	$cekMember = $this->db->query("select * from member where kode_member = '$kode_member' ");
	$resMem = $cekMember->result();
	if($cekMember->num_rows() > 0): 
		$this->session->set_flashdata("message","Maaf Kode Member Sudah Ada");
		redirect('admin');
		
	else:	
		$this->db->insert('member',$data);
		$this->session->set_flashdata("message","Data sudah Masuk");
		redirect('admin');
	endif;
	}
	public function deleteMember($id){
		$this->db->where('id_member',$id);
		$this->db->delete("member");
		redirect("admin");
	}
	/*====================================*/
	/*Bagian Karyawan*/
	/*====================================*/

	public function listKaryawan(){
		$data['isi']=$this->Member_model->getKaryawan();
		$data['content']="backend/karyawanList";/* View */
		$this->load->view('template/content',$data);
	}
	public function getKaryawanId($id){
		$data['qry']=$this->Member_model->getKaryawanId($id);
		$data['content']="backend/karyawanView";/* View */
		$this->load->view('template/content',$data);
	}
	public function editKaryawan($id){
		$data['qry']=$this->Member_model->getKaryawans($id);
		$data['jabatan']=$this->Member_model->getJabatan();
		$data['content']="backend/karyawanEdit";/* View */
		$this->load->view('template/content',$data);
	}
	public function createKaryawan(){
		$data['jabatan']=$this->Member_model->getJabatan();
		$data['content']="backend/karyawanCreate";/* View */
		$this->load->view('template/content',$data);
	}
	public function doEditKaryawan($id){
		
		$name =  $this->input->post('name');
		$nama_jabatan = $this->input->post('nama_jabatan');
		$ktp = 	$this->input->post('ktp');
		$address =	$this->input->post('address');
		$address2 =	$this->input->post('address2');
		$tempt_lahir =	$this->input->post('tempt_lahir');
		$no_telp = $this->input->post('no_telp');
		$no_hp = $this->input->post('no_hp');
		$data = array(
				'name' => $name,
				'id_jabatan' => $nama_jabatan,
				'ktp' => $ktp,
				'address' => $address,
				'address2' => $address2,
				'tempt_lahir' => $tempt_lahir,
				'no_telp' => $no_telp,
				'no_hp' =>$no_hp,
			);
		$this->db->where('id', $id);
		$this->db->update('karyawan',$data);
		echo "<script>alert('Edit data karyawan berhasil');
			window.location.href= '".base_url()."';
			</script>";
		//redirect("admin/listKaryawan");
	}
	public function doCreateKaryawan(){
		
		$name =  $this->input->post('name');
		$nama_jabatan = $this->input->post('nama_jabatan');
		$ktp = 	$this->input->post('ktp');
		$address =	$this->input->post('address');
		$address2 =	$this->input->post('address2');
		$tempt_lahir =	$this->input->post('tempt_lahir');
		$no_telp = $this->input->post('no_telp');
		$tgl_lahir = $this->input->post('tgl_lahir');
		$no_hp = $this->input->post('no_hp');
		$data = array(
				'name' => $name,
				'id_jabatan' => $nama_jabatan,
				'ktp' => $ktp,
				'address' => $address,
				'address2' => $address2,
				'tempt_lahir' => $tempt_lahir,
				'no_telp' => $no_telp,
				'tgl_lahir' => $tgl_lahir,
				'no_hp' =>$no_hp,
			);
		$this->db->insert('karyawan',$data);
		echo "<script>alert('input data karyawan berhasil');
			window.location.href= '".base_url()."';
			</script>";
		// redirect("admin/listKaryawan");

	}
	public function deleteKaryawan($id){
		$this->db->where('id',$id);
		$this->db->delete("karyawan");
		echo "<script>alert('Data berhasil di hapus');
			window.location.href= '".base_url()."';
			</script>";
		// redirect("admin");
	}
	/*===================*/
	/*Time_Work*/
	/*===================*/
	public function listTimework(){
		$data['isi']=$this->Member_model->getTimework();
		$data['content']="backend/timeworkList";/* View */
		$this->load->view('template/content',$data);
	}
	public function editTimework($id){
		$data['qry']=$this->Member_model->getTimeworkId($id);
		$data['content']="backend/timeworkEdit";/* View */
		$this->load->view('template/content',$data);
	}
	public function doEditTimework($id){
		$senin = $this->input->post('senin');	
		$selasa = $this->input->post('selasa');	
		$rabu = $this->input->post('rabu');	
		$kamis = $this->input->post('kamis');	
		$jumat = $this->input->post('jumat');	
		$sabtu = $this->input->post('sabtu');	
		$minggu = $this->input->post('minggu');	
		$jam_masuk = $this->input->post('jam_masuk');	
		$jam_keluar = $this->input->post('jam_keluar');	
		$data = array(
				'senin'=>$senin,
				'selasa'=>$selasa,
				'rabu'=>$rabu,
				'kamis'=>$kamis,
				'jumat'=>$jumat,
				'sabtu'=>$sabtu,
				'minggu'=>$minggu,
				'jam_masuk'=>$jam_masuk,
				'jam_keluar'=>$jam_keluar,
			);
		$this->db->where('id',$id);
		$this->db->update('time_work',$data);
		redirect("admin/listTimework");
	}
	public function doCreateTimework(){
		$id_karyawan = $this->input->post('id_karyawan');	
		$senin = $this->input->post('senin');	
		$selasa = $this->input->post('selasa');	
		$rabu = $this->input->post('rabu');	
		$kamis = $this->input->post('kamis');	
		$jumat = $this->input->post('jumat');	
		$sabtu = $this->input->post('sabtu');	
		$minggu = $this->input->post('minggu');	
		$jam_masuk = $this->input->post('jam_masuk');	
		$jam_keluar = $this->input->post('jam_keluar');	
		$data = array(
				'id_karyawan'=>$id_karyawan,
				'senin'=>$senin,
				'selasa'=>$selasa,
				'rabu'=>$rabu,
				'kamis'=>$kamis,
				'jumat'=>$jumat,
				'sabtu'=>$sabtu,
				'minggu'=>$minggu,
				'jam_masuk'=>$jam_masuk,
				'jam_keluar'=>$jam_keluar,
			);
		$this->db->insert('time_work',$data);
		redirect("admin/listTimework");
	}
	public function createTimework(){
		$data['karyawan']=$this->Member_model->getTimekaryawan();
		$data['content']="backend/timeworkCreate";/* View */
		$this->load->view('template/content',$data);
	}
	public function deleteTimework($id){
		$this->db->where('id',$id);
		$this->db->delete("time_work");
		redirect("admin/listTimework");
	}
};
