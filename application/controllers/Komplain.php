<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Komplain extends CI_Controller {


	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct(){
		parent::__construct();
		$this->load->model("Member_model");
		if(!$this->session->userdata('logged_in')){
			redirect('login');
		}
	}
	public function index()
	{
		$this->listComplaint();
	}
	//komplain sudah
	public function Complaintsudah()
	{
		
		$data['isi']=$this->Member_model->getKomplain()->result();
		//var_dump($data['isi']);
		$data['content']="backend/complaintsudah";
		$this->load->view('template/content',$data);
	}
	//komplain sudah
	public function Complaintbelum()
	{
		
		$data['isi']=$this->Member_model->getKomplain()->result();
		//var_dump($data['isi']);
		$data['content']="backend/complaintbelum";
		$this->load->view('template/content',$data);
	}
	//komplain sudahbelum
	public function Complaintsudahblm()
	{
		
		$data['isi']=$this->Member_model->getKomplain()->result();
		//var_dump($data['isi']);
		$data['content']="backend/complaintsudahblm";
		$this->load->view('template/content',$data);
	}
	public function listComplaint()
	{
		
		$data['isi']=$this->db->from('komplainCabang')
            ->select(['komplainCabang.*','cabang.namaCabang','t_kategori_komplain.nm_kategori','karyawan.name as nama_karyawan'])
            ->join('cabang','cabang.id = komplainCabang.id_cabang','left')
            ->join('t_kategori_komplain','t_kategori_komplain.id_kategori = komplainCabang.id_kategori','left')
            ->join('karyawan','karyawan.id = komplainCabang.created_by','left')
            ->get()->result();
//		echo"<pre>";print_r($data['isi']);exit;
//		$data['isi']=$this->Member_model->getKomplain()->result();
		//var_dump($data['isi']);
		$data['content']="backend/komplain/complaintList";
		$this->load->view('template/content',$data);
	}
	public function answareComplaint($id)
	{
		if($this->session->userdata('hak_akses')==3){
			redirect('complaint');
		}
		$data['isi']=$this->db->from('komplainCabang')
            ->select(['komplainCabang.*','cabang.namaCabang','t_kategori_komplain.nm_kategori','karyawan.name as nama_karyawan'])
            ->join('cabang','cabang.id = komplainCabang.id_cabang','left')
            ->join('t_kategori_komplain','t_kategori_komplain.id_kategori = komplainCabang.id_kategori','left')
            ->join('karyawan','karyawan.id = komplainCabang.created_by','left')
            ->where(['komplainCabang.id'=>$id])
            ->get()->result();
		$data['isi1']=$this->db->from('komplainJawaban')->where(['komplain_id'=>$id])->get()->result();
//		var_dump($data['isi1']);exit;
		$data['content']="backend/komplain/complaintAnsware";
		$this->load->view('template/content',$data);
	}
	public function answareComplaintList($id)
	{
		$data['isi']=$this->Member_model->getKomplainId($id)->result();
		$data['isi1']=$this->Member_model->getTanggapanId($id)->result();
		
		//var_dump($data['isi']);
		$data['content']="backend/complaintAnswareList";
		$this->load->view('template/content',$data);
	}
	public function doAnswareComplaint(){
		$id_karyawan = $this->input->post('id_karyawan');
		$id_komplain = $this->input->post('id_komplain');
//		$isi_tanggapan = $this->input->post('isi_tanggapan');
		$case_link = $this->input->post('case_link');
		$root_cause = $this->input->post('root_cause');
		$impact = $this->input->post('impact');
		$action = $this->input->post('action');
		$down_time = $this->input->post('down_time');
		$up_time = $this->input->post('up_time');
		$data = array(
			'user_id' => $id_karyawan,
			'komplain_id' => $id_komplain,
			'root_cause' => $root_cause,
			'case_link' => $case_link,
			'impact' => $impact,
			'action' => $action,
			'downtime' => date('H:i:s',strtotime($down_time)),
			'uptime' => date('H:i:s',strtotime($up_time)),
			'created_ad' => date("Y-m-d H:i:s"),
			);
		$this->db->insert("komplainJawaban",$data);

		$data1 = array(
			'active' => 2
			);
		$this->db->where('id',$id_komplain);
		$this->db->update('komplainCabang',$data1);
		redirect('complaint');
	}
	function createComplaint(){
	    $data['cabang']=$this->db->from('cabang')->get()->result();
		$data['kategori'] = $this->db->from('t_kategori_komplain')->get()->result();
		$data['content']="backend/komplain/createComplaint";
		$this->load->view('template/content',$data);
	}
	function lapComplaint(){
		$where =' ';
		$tglawal='';
		$tglakhir='';
		if($_POST){
			$tglawal=$this->input->post('tglawal');
			$tglakhir=$this->input->post('tglakhir');
			$where .= " where date(date_created) between '$tglawal' and '$tglakhir' ";
		}
		$data['isi'] = $this->Member_model->getTotalKomplain($where);
		$data['tglawal'] = $tglawal;
		$data['tglakhir'] = $tglakhir;
		$data['content']="backend/lapKomplain";
		$this->load->view('template/content',$data);
	}
	function findcomplaint($idkategori=null, $tglawal=null,$tglakhir=null){
		$data['isi'] = $this->Member_model->getKomplainfind($idkategori, $tglawal,$tglakhir)->result();
		$data['content']="backend/complaintFind";
		$this->load->view('template/content',$data);
	}
	function doInsertKomplain(){
	    if($_POST){
	        $req = $_POST;
	        $cabangid = $req['cabangid'];
	        $kategoriid = $req['kategoriid'];
	        $isikomplain = $req['isikomplain'];
	        $prio = $req['prio'];
	        $idkarywan = $this->session->userdata('id_karyawan');
	        $datainsert = array(
	            'id_cabang'=>$cabangid,
	            'id_kategori'=>$kategoriid,
	            'isi_komplain'=>$isikomplain,
	            'prioritas'=>$prio,
	            'created_by'=>$idkarywan,
                'date_created'=>date('Y-m-d H:i:s'),
                'active'=>1
            );
	        $this->db->insert('komplainCabang',$datainsert);
	        return redirect('komplain');
        }
    }
}


