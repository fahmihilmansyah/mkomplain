<div class="container">
    <div class="bs-example bs-example-tabs" data-example-id="togglable-tabs">
    <ul id="myTabs" class="nav nav-tabs" role="tablist">
      <li class="active"><a data-toggle="tab" href="#home">Home</a></li>
      <li ><a data-toggle="tab" href="#complaint">Komplain</a></li>
      <li class="rescomplaint"><a data-toggle="tab" href="#complaintdata">Data Komplain</a></li>
      <li class="rescomplaintT"><a data-toggle="tab" href="#complaintansware">Answare Komplain</a></li>
	   <li ><a data-toggle="tab" href="#promotion">Promotion</a></li>
     <li ><a data-toggle="tab" href="#event">Event of GHS</a></li>
    </ul>
    <div id="myTabContent" class="tab-content">
    <div role="tabpanel" class="tab-pane fade active in" id="home" >
      <div class="container">
            <h2>
            Selamat datang di web Griya Hemat
            </h2>
            <div class="row">
            <div class="col-md-4">
            <img src="<?php echo base_url()?>assets/hemat.jpg" width=100% alt="">
            </div>
            <div class="col-md-8">
                <div class="content">
                  <div class="indent">
                    <p class="alt-top">GRIYA Hemat Soekarno Hatta adalah cabang YOGYA GROUP yang ke 51. Konsep GRIYA Hemat adalah menjual barang-barang secara grosir atau whole saler , tetapi tetap terbuka bagi end-user,saat ini GRIYA Hemat hanya ada satu cabang,yaitu GRIYA Hemat Soekarno Hatta  (GHS).
                GRIYA Hemat Soekarno Hatta dibuka pada tanggal 19 September 2007, toko yang menggunakan konsep HEMAT pertama, yang membedakan dengan toko-toko lain yaitu tersedianya harga kartonan dimana harga kartonan lebih murah dari harga satuannya.Di GRIYA Hemat Soekarno Hatta menjual produk-produk supermarket juga fashion, dengan luas tanah/bangunan sekitar 1400 m2 dan luas area 1200 m2. Tetapi sekarang GRIYA Hemat menjual kartonan harganya  sama dengan  harga eceran.Di GRIYA Hemat juga menjual roti- roti yang  dibuat oleh Bread Co,ada food court yang dimasak oleh  karyawan (koki) sendiri.</p>
                    <div class="clear"></div>
                    <div class="line-hor"></div>
                    <div class="wrapper line-ver">
                      <div class="col-1">
                        <h3>Struktur Organisasi Griya Hemat</h3>
                        <ul>
                          <li>Store Manager   : Pak Victor Djojohartono</li>
                          <li>Personalia  : Ibu Dewi Riyanti</li>
                          <li>Staff Keuangan  : Ibu Febrina Siska</li>
                          <li>Staff EDP   : Saepunajat</li>
                          <li>Staff Buyer   : Ibu Agustina </li>
                          <li>Staff Receiving   : Pak Papo Poernama</li>
                          <li>Spv. Fresh  : PakUtep Wahyudin</li>
                          <li>Spv. Food   : Pak Idvan</li>
                          <li>Spv. Non Food   : Ibu Aida Fitriani</li>
                          <li>Spv. GMS  : Pak Djuharsa</li>
                        </ul>
                       
                      </div>
                      <div class="col-2">
                        <h3>Location</h3>
                        <p>Griya Hemat Soekarno Hatta <br>Jalan Soekarno Hatta No.335 Buah Batu </p>
                        <dl class="contacts-list">
                          <dt>Bandung</dt>
                          <dd>022-7503344</dd>
                          <dd>&nbsp;</dd>
                        </dl>
                      </div>
                    </div>
                  </div>
                </div>
            </div>
            </div>
      </div>
        </div><!-- masukan gambar promosi-->
   <div role="tabpanel" class="tab-pane fade  in" id="promotion"> <h3>Promo Harga Heran</h3>
	 <img src="<?php echo base_url()?>assets/Harga-Heran.jpg" alt=""> <h3>Promo Serba Hemat</h3>
    <img src="<?php echo base_url()?>assets/sermat1.jpg" alt=""><br>
    <img src="<?php echo base_url()?>assets/sermat2.jpg" alt=""><br>
    <img src="<?php echo base_url()?>assets/sermat2.jpg" alt="">
	 </div><!-- masukan gambar acara-->
   <div role="tabpanel" class="tab-pane fade  in" id="event">
   <img src="<?php echo base_url()?>assets/acara1.jpg" alt="">
   <img src="<?php echo base_url()?>assets/acara2.jpg" alt="">
   <img src="<?php echo base_url()?>assets/acara3.jpg" alt="">
   </div>
      <div role="tabpanel" class="tab-pane fade  in" id="complaint">
      <form method='post' action="<?php echo base_url()?>member/doInsertKomplain">
        <div class="row" >
        <div class="form-group">
          <label for="kdMember">Kode Member</label><input autocomplete="off" class="form-control" type="text" name="kdMember" id="kdMember" value="" placeholder="Please Input Your Member Code">
          <label id="chkkd" class="btn btn-warning">Check</label>
        </div>
        </div>
        <hr>
        <div class="row">
          <div class="result">
            <div class="form-group">
              <div class="rexs"></div>
              <label for="">Name:</label> <label><span class="nma"></span></label><br>
              <label for="nmEmail">Email:</label><input class="form-control" type="email" name="nmEmail" readonly id="nmEmail" value="" placeholder="Please Input Your Email">
              <label for="kdKat">Kategori</label>
                <select class="form-control" name="kdKat" id="kdKat">
                  <option value="0">--Pilih Kategori--</option>
                  <?php foreach($isi as $row): ?>
                    <option value="<?php echo $row->id_kategori ?>"><?php echo $row->nm_kategori ?></option>
                  <?php endforeach; ?>
                </select>
              <label for="nmComp">Complaint:</label><textarea class="form-control" name="nmComp" id="nmComp" value="" placeholder="Please Input Your Complaint"></textarea>
              <button type="submit" class="btn btn-success">Submit Complaint</button>
            </div>
          </div>
        </div>
      </form>
      </div><!-- end of tab complaint-->
      <div role="tabpanel" class="tab-pane fade " id="complaintdata" >
        <table class="table table-striped">
          <thead>
            <tr>
              <th>No. Komplain</th>
              <th>Jenis Komplain</th>
              <th>Content</th>
              <th>Tanggal</th>
              <th>Status</th>
            </tr>
          </thead>
          <tbody class="complaintdatas">
            
          </tbody>
        </table>
      </div>
      
      </div>
      
    </div>
    </div>