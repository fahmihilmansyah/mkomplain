<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title></title>
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.css">
  <script src="<?php echo base_url(); ?>assets/js/bootstrap.js" type="text/javascript" ></script>
  <script src="<?php echo base_url(); ?>assets/js/jquery.js" type="text/javascript" ></script>
  <script src="<?php echo base_url(); ?>assets/js/dropdown.js" type="text/javascript" ></script>
  <script src="<?php echo base_url(); ?>assets/js/modal.js" type="text/javascript" ></script>
	<script src="<?php echo base_url(); ?>assets/js/tab.js" type="text/javascript" ></script>
  <script type="text/javascript">
  $(document).ready(function(){
    var urlist = "<?php echo base_url()?>";
    $('.rescomplaint').hide();
    $('.rescomplaintT').hide();
    $('#myTabs a').click(function (e) {
      e.preventDefault()
      $(this).tab('show')
    });
    $("#chkkd").click(function(){
      $.ajax({
            url: urlist+"member/checkMember",
            type :'POST',
            data : {
                    kode_member: $('#kdMember').val(), 
                  },
            dataType : 'json',
            cache : false,/
            beforeSend:function(){
              $('.rexs').empty(); 
            },
            success:function(msg){              
                  $('.nma').html(msg.isi.nama);
                  console.log("Kode Member: "+msg.isi.idmember+"");
                  $('.rexs').append("<input type='hidden' name='idmember' id='idmembers' value='"+msg.isi.idmember+"'>");
                  $('#nmEmail').val(msg.isi.email);
                  getComplaint(msg.isi.idmember);
                  $('.rescomplaint').show();
                }else{
                  alert("false");
                }
              }
          });
    });
    function getComplaint(id){
      $.ajax({
            url: urlist+"member/checkKomplain",
            type :'POST',
            data : {
                    kode_member: id,
                  },
            dataType : 'json',
            beforeSend:function(){
              $(".complaintdatas").empty();
            },
            success:function(msg){   
            var isi ="";           
            var actived="";         
                  $.each(msg.isi, function(){
                    console.log(this.nama);
                    console.log(this.isi_komplain);
                    if(this.active == 1){
                      actived = "<label class='btn btn-success'>Active</label>";
                    }
                    if(this.active == 2){
                      actived = "<a href='"+urlist+"member/getComplaintDetail/"+this.id_komplain+"/"+id+"' class='repl btn btn-warning' data-idkom='"+this.id_komplain+"'  >Replied</a> ";
                    }
                    isi += "<tr>"+
                    "<td>"+this.id_komplain+"</td>"+
                    "<td>"+this.nm_kategori+"</td>"+
                    "<td>"+this.isi_komplain+"</td>"+
                    "<td>"+this.date_created+"</td>"+
                    "<td>"+actived+"</td>"+
                    "</tr>";
                  });
                    $(".complaintdatas").append(isi);
              }
          });
    }
    
    
  });
  </script>
</head>
<body>

      <div class="container">
       <div class="header clearfix">
        <nav>
          <ul class="nav nav-pills pull-right">
            <li role="presentation" class="active"><a href="#">Home</a></li>
            <li role="presentation"><a href="#">About</a></li>
            <li role="presentation"><a href="#">Contact</a></li>
          </ul>
        </nav>
        <h3 class="text-muted">GRIYA HEMAT</h3>
      </div>

    <!-- Main jumbotron for a primary marketing message or call to action -->
    <div class="jumbotron">
      <div class="container">
        <p>This is a template for a simple marketing or informational website. It includes a large callout called a jumbotron and three supporting pieces of content. Use it as a starting point to create something more unique.</p>
      </div>
    </div>

    <div class="container">
    <div class="bs-example bs-example-tabs" data-example-id="togglable-tabs">
    <ul id="myTabs" class="nav nav-tabs" role="tablist">
      <li class="active"><a data-toggle="tab" href="#complaint">Complaint</a></li>
      <li class="rescomplaint"><a data-toggle="tab" href="#complaintdata">Data Complaint</a></li>
      <li class="rescomplaintT"><a data-toggle="tab" href="#complaintansware">Answare Complaint</a></li>
    </ul>
    <div id="myTabContent" class="tab-content">
      <div role="tabpanel" class="tab-pane fade active in" id="complaint">
      <form method='post' action="<?php echo base_url()?>member/doInsertKomplain">
        <div class="row" >
        <div class="form-group">
          <label for="kdMember">Kode Member</label><input autocomplete="off" class="form-control" type="text" name="kdMember" id="kdMember" value="" placeholder="Please Input Your Member Code">
          <label id="chkkd" class="btn btn-warning">Check</label>
        </div>
        </div>
        <hr>
        <div class="row">
          <div class="result">
            <div class="form-group">
              <label for="kdKat">Kategori</label>
                <select class="form-control" name="kdKat" id="kdKat">
                  <option value="0">--Pilih Kategori--</option>
                  <?php foreach($isi as $row): ?>
                    <option value="<?php echo $row->id_kategori ?>"><?php echo $row->nm_kategori ?></option>
                  <?php endforeach; ?>
                </select>
                <div class="rexs"></div>
              <label for="">Name:</label> <label><span class="nma"></span></label><br>
              <label for="nmEmail">Email:</label><input class="form-control" type="email" name="nmEmail" readonly id="nmEmail" value="" placeholder="Please Input Your Email">
              <label for="nmComp">Complaint:</label><textarea class="form-control" name="nmComp" id="nmComp" value="" placeholder="Please Input Your Complaint"></textarea>
              <button type="submit" class="btn btn-success">Submit Complaint</button>
            </div>
          </div>
        </div>
      </form>
      </div><!-- end of tab complaint-->
      <div role="tabpanel" class="tab-pane fade " id="complaintdata" >
        <table class="table table-striped">
          <thead>
            <tr>
              <th>No. Komplain</th>
              <th>Jenis Komplain</th>
              <th>Content</th>
              <th>Tanggal</th>
              <th>Status</th>
            </tr>
          </thead>
          <tbody class="complaintdatas">
            
          </tbody>
        </table>
      </div>
      <div role="tabpanel" class="tab-pane fade " id="complaintansware" >
          <table class="table">
          <thead>
            <tr>
              <th>header</th>
              <th>header</th>
              <th>header</th>
            </tr>
          </thead>
          <tbody class="complaintAnsware">
            <tr>
              <td>data</td>
              <td>data</td>
              <td>data</td>
            </tr>
          </tbody>
        </table>
        </div>
      </div>
      
    </div>
    </div>
    </div><!--- endd container -->
    <hr>
      <div class="container">
      <footer>
        <p>&copy; Griya Hemat 2015</p>
      </footer>
    </div> <!-- /container -->
</body>
</html>