<div class="group-form">
	<h2>Answare Komplen</h2>
	<hr>
</div>
<table class="table ">
	<thead>
		<tr>
			<th>Kode Member</th>
			<th>Nama Member</th>
			<th>Kriteria Komplain</th>
			<th>Isi Komplen</th>
			<th>Tanggal Komplen</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach($isi as $row):?>
		<tr>
			<td><?php echo $row->kode_member?></td>
			<td><?php echo $row->nama?></td>
			<td><?php echo $row->nm_kategori?></td>
			<td><?php echo $row->isi_komplain?></td>
			<td><?php echo $row->date_created?></td>
		</tr>
	<?php endforeach; ?>
	</tbody>
</table>
<br>
<hr>
<div class="container">
	<div class="row">
		<div class="form-group">
		<form action="<?php echo base_url()?>complaint/doAnswareComplaint" method="post">
			<label>Karyawan: </label>&nbsp;<label><?php echo $this->session->userdata('name');?></label>
			<input type="hidden" name="id_karyawan" value="<?php echo $this->session->userdata('id_karyawan');?>">
			<br>
			<input type="hidden" name="id_komplain" value="<?php echo $isi[0]->id?>" >
			<label for="tanggapan">Tanggapan Komplain:</label>
			<textarea id="tanggapan" name="isi_tanggapan" class="form-control" placeholder="Masukkin Tanggapan"></textarea>
			<button class="form-control btn btn-success">Simpan</button>
		</form>
		</div>
	</div>
</div>