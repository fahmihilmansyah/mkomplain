<div class="form-group">
	<h2>List Waktu Kerja</h2>
	<hr>
	<a href="<?php echo base_url()?>admin/createTimework" class="btn btn-warning">Create waktu kerja</a>
</div>
<table class="table">
	<thead>
		<tr>
			<th class="text-center">Nama Karyawan</th>
			<th class="text-center">Senin</th>
			<th class="text-center">Selasa</th>
			<th class="text-center">Rabu</th>
			<th class="text-center">Kamis</th>
			<th class="text-center">Jumat</th>
			<th class="text-center">Sabtu</th>
			<th class="text-center">Minggu</th>
			<th class="text-center">Jam Masuk</th>
			<th class="text-center">Jam Keluar</th>
			<th class="text-center">Action</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($isi as $row):?>
		<tr>
			<td><?php echo $row->name?></td>
			<td><?php echo $row->senin == 1 ?"<label class='btn btn-primary'>ADA</label>":"<label class='btn btn-danger'>TIDAK ADA</label>"?></td>
			<td><?php echo $row->selasa == 1 ?"<label class='btn btn-primary'>ADA</label>":"<label class='btn btn-danger'>TIDAK ADA</label>"?></td>
			<td><?php echo $row->rabu == 1 ?"<label class='btn btn-primary'>ADA</label>":"<label class='btn btn-danger'>TIDAK ADA</label>"?></td>
			<td><?php echo $row->kamis == 1 ?"<label class='btn btn-primary'>ADA</label>":"<label class='btn btn-danger'>TIDAK ADA</label>"?></td>
			<td><?php echo $row->jumat == 1 ?"<label class='btn btn-primary'>ADA</label>":"<label class='btn btn-danger'>TIDAK ADA</label>"?></td>
			<td><?php echo $row->sabtu == 1 ?"<label class='btn btn-primary'>ADA</label>":"<label class='btn btn-danger'>TIDAK ADA</label>"?></td>
			<td><?php echo $row->minggu == 1 ?"<label class='btn btn-primary'>ADA</label>":"<label class='btn btn-danger'>TIDAK ADA</label>"?></td>
			<td><?php echo $row->jam_masuk?></td>
			<td><?php echo $row->jam_keluar?></td>
			<td class="text-center"> <a href="<?php echo base_url() ?>admin/editTimework/<?php echo $row->id?>" class="btn btn-success">Edit</a> || <a href="<?php echo base_url() ?>admin/deleteTimework/<?php echo $row->id?>" class="btn btn-warning">Delete</a></td>
		</tr>
	<?php endforeach; ?>
	</tbody>
</table>
