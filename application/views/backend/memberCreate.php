<div class="form-group">
	<h2>Create Member</h2>
</div>
<form method="post" action="<?php echo base_url();?>Admin/doInsertMember/">
<table class="table">
	<tr>
		<td>Kode Member</td><td><input type="text" name="kode_member" value="0206" placeholder="Kode Member"> </td>
	</tr>
	<tr>		
		<td>Nama</td><td><input type="text" name="nama" value="" placeholder="Nama Member"> </td>
	</tr>
	<tr>		
		<td>KTP / SIM</td><td><input type="number" name="no_ktp_sim" value="" placeholder="Ktp / SIM"> </td>
	</tr>
	<tr>		
		<td>Alamat</td><td><textarea name="alamat" placeholder="Alamat"></textarea>
		</td>
	</tr>
	<tr>		
		<td>Email</td><td><input type="text" name="email" value="" placeholder="Email"> </td>
	</tr>
	<tr>		
		<td>Tempat, Tgl Lahir</td><td><input type="text" name="lahir" value="" placeholder="Tempat Lahir"> <input type="date" name="tanggal_lahir" value="" placeholder="Tanggal Lahri: yyyy-mm-dd"> </td>
	</tr>
	<tr>		
		<td>Ahli Waris</td><td><input type="text" name="ahli_waris" value="" placeholder="Ahli Waris"> </td>
	</tr>
	<tr>		
		<td>Agama</td><td>
		<select name="agama">
					<?php foreach($agama as $ragama): ?>
					<option value="<?php echo $ragama->id ?>"  ><?php echo $ragama->agama;?></option>
				<?php endforeach;?>
		</select>
		</td>
	</tr>
	<tr>		
		<td>No. Telp</td><td><input type="text" name="no_tlp" value="" placeholder="No Telp"> </td>
	</tr>
	<tr>		
		<td>Status Perkawinan</td><td>
		<select name="status">
					<?php foreach($status as $rstatus): ?>
					<option value="<?php echo $rstatus->id ?>" ><?php echo $rstatus->statusPerkawinan;?></option>
				<?php endforeach;?>
		</select>
		</td>
	</tr>
	<tr>		
		<td>Jenis Kelamin</td><td>
			<select name="gender">
					<?php foreach($gender as $rgender): ?>
					<option value="<?php echo $rgender->id ?>"  ><?php echo $rgender->gender;?></option>
				<?php endforeach;?>
			</select>
		</td>
	</tr>
	<tr>		
		<td>Pendidikan</td><td>
		<!-- <input type="text" name="pendidikan" value="" placeholder="Pendidikan"> -->
		<select name="pendidikan">
			<option value="SMP" >SMP</option>
			<option value="SMA" >SMA</option>
			<option value="D3" >D3</option>
			<option value="S1" >S1</option>
			<option value="OTHER" >OTHER</option>
		</select> 
		</td>
	</tr>
	<tr>		
		<td>Pekerjaan</td><td>
			<select name="pekerjaan">
				<?php foreach($pekerjaan as $rpekerjaan): ?>
				<option value="<?php echo $rpekerjaan->id ?>"  ><?php echo $rpekerjaan->pekerjaan;?></option>
			<?php endforeach;?>
			</select>
		</td>
	</tr>
	<tr><td colspan="2" class="text-center"><button type="submit" class="btn btn-success">Simpan</button> ||<button type="reset" class="btn btn-danger">Reset</button> || <a href="<?php echo base_url();?>admin" class="btn btn-warning">Back</a></td></tr>
</table>
</form>
