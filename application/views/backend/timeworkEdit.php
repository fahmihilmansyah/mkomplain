<div>
	<h2>Edit Waktu Kerja</h2>
</div>
<form method="post" action="<?php echo base_url();?>admin/doEditTimework/<?php echo $qry[0]->id_karyawan;?>">
<table class="table ">
		<tr>
			<td>Karyawan</td><td><?php echo $qry[0]->name ?>
			</td>
		</tr>
		<tr>
			<td>Senin</td><td>
				<div class="form-group">
					<input type="radio" name="senin" value="1" <?php echo $qry[0]->senin == 1?"checked":""; ?> >Ada
					<input type="radio" name="senin" value="0" <?php echo $qry[0]->senin == 0?"checked":""; ?>>Tidak Ada
				</div>
			</td>
		</tr>
		<tr>
			<td>Selasa</td><td>
				<div class="form-group">
					<input type="radio" name="selasa" value="1" <?php echo $qry[0]->selasa == 1?"checked":""; ?> >Ada
					<input type="radio" name="selasa" value="0" <?php echo $qry[0]->selasa == 0?"checked":""; ?>>Tidak Ada
				</div>
			</td>
		</tr>
		<tr>
			<td>Rabu</td><td>
				<div class="form-group">
					<input type="radio" name="rabu" value="1" <?php echo $qry[0]->rabu == 1?"checked":""; ?> >Ada
					<input type="radio" name="rabu" value="0" <?php echo $qry[0]->rabu == 0?"checked":""; ?>>Tidak Ada
				</div>
			</td>
		</tr>
		<tr>
			<td>Kamis</td><td>
				<div class="form-group">
					<input type="radio" name="kamis" value="1" <?php echo $qry[0]->kamis == 1?"checked":""; ?> >Ada
					<input type="radio" name="kamis" value="0" <?php echo $qry[0]->kamis == 0?"checked":""; ?>>Tidak Ada
				</div>
			</td>
		</tr>
		<tr>
			<td>Jumat</td><td>
				<div class="form-group">
					<input type="radio" name="jumat" value="1" <?php echo $qry[0]->jumat == 1?"checked":""; ?> >Ada
					<input type="radio" name="jumat" value="0" <?php echo $qry[0]->jumat == 0?"checked":""; ?>>Tidak Ada
				</div>
			</td>
		</tr>
		<tr>
			<td>Sabtu</td><td>
				<div class="form-group">
					<input type="radio" name="sabtu" value="1" <?php echo $qry[0]->sabtu == 1?"checked":""; ?> >Ada
					<input type="radio" name="sabtu" value="0" <?php echo $qry[0]->sabtu == 0?"checked":""; ?>>Tidak Ada
				</div>
			</td>
		</tr>
		<tr>
			<td>Minggu</td><td>
				<div class="form-group">
					<input type="radio" name="minggu" value="1" <?php echo $qry[0]->minggu == 1?"checked":""; ?> >Ada
					<input type="radio" name="minggu" value="0" <?php echo $qry[0]->minggu == 0?"checked":""; ?>>Tidak Ada
				</div>
			</td>
		</tr>
		<tr>
			<td>Jam Masuk</td><td><input type="time" name="jam_masuk" value="<?php echo $qry[0]->jam_masuk?>" placeholder="hh-mm-ss"></td>
		</tr>
		<tr>
			<td>Jam Keluar</td><td><input type="time" name="jam_keluar" value="<?php echo $qry[0]->jam_keluar?>" placeholder="hh-mm-ss"></td>
		</tr>
		<tr><td colspan="2" class="text-center"><button class="btn btn-success" type="submit">Simpan</button> || <a class="btn btn-danger" href="<?php echo base_url()?>admin/listTimework">Back</a></td></tr>
</table>
</form>
