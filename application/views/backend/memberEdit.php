<div class="form-group">
	<h2>Edit Member</h2>
</div>
<form method="POST" action="<?php echo base_url();?>Admin/doEditMember/<?php echo $qry[0]->id_member?>">
<table class="table">
	<tr>
		<td>Kode Member</td><td><input type="text" name="kode_member" value="<?php echo $qry[0]->kode_member ?>" placeholder=""> </td>
	</tr>
	<tr>		
		<td>Nama</td><td><input type="text" name="nama" value="<?php echo $qry[0]->nama ?>" placeholder=""> </td>
	</tr>
	<tr>		
		<td>KTP / SIM</td><td><input type="text" name="no_ktp_sim" value="<?php echo $qry[0]->no_ktp_sim ?>" placeholder=""> </td>
	</tr>
	<tr>		
		<td>Alamat</td><td><textarea name="alamat" placeholder="Alamat"><?php echo $qry[0]->alamat ?></textarea>
		</td>
	</tr>
	<tr>		
		<td>Email</td><td><input type="text" name="email" value="<?php echo $qry[0]->email ?>" placeholder=""> </td>
	</tr>
	<tr>		
		<td>Tempat, Tgl Lahir</td><td><input type="text" name="lahir" value="<?php echo $qry[0]->lahir?>" placeholder=""> <input type="date" name="tanggal_lahir" value="<?php echo $qry[0]->tanggal_lahir ?>" placeholder=""> </td>
	</tr>
	<tr>		
		<td>Ahli Waris</td><td><input type="text" name="ahli_waris" value="<?php echo $qry[0]->ahli_waris ?>" placeholder=""> </td>
	</tr>
	<tr>		
		<td>Agama</td><td>
		<select name="agama">
					<?php foreach($agama as $ragama): ?>
					<option value="<?php echo $ragama->id ?>" <?php echo ($ragama->id == $qry[0]->idagama)?"selected":"";?> ><?php echo $ragama->agama;?></option>
				<?php endforeach;?>
		</select>
		</td>
	</tr>
	<tr>		
		<td>No. Telp</td><td><input type="text" name="no_tlp" value="<?php echo $qry[0]->no_tlp ?>" placeholder=""> </td>
	</tr>
	<tr>		
		<td>Status Perkawinan</td><td>
		<select name="status">
					<?php foreach($status as $rstatus): ?>
					<option value="<?php echo $rstatus->id ?>" <?php echo ($rstatus->id == $qry[0]->idstatus)?"selected":"";?> ><?php echo $rstatus->statusPerkawinan;?></option>
				<?php endforeach;?>
		</select>
		</td>
	</tr>
	<tr>		
		<td>Jenis Kelamin</td><td>
			<select name="gender">
					<?php foreach($gender as $rgender): ?>
					<option value="<?php echo $rgender->id ?>" <?php echo ($rgender->id == $qry[0]->idgender)?"selected":"";?> ><?php echo $rgender->gender;?></option>
				<?php endforeach;?>
			</select>
		</td>
	</tr>
	<tr>		
		<td>Pendidikan</td><td>
		<input type="text" name="pendidikan" value="<?php echo $qry[0]->pendidikan ?>" placeholder=""> </td>
	</tr>
	<tr>		
		<td>Pekerjaan</td><td>
			<select name="pekerjaan">
				<?php foreach($pekerjaan as $rpekerjaan): ?>
				<option value="<?php echo $rpekerjaan->id ?>" <?php echo ($rpekerjaan->id == $qry[0]->idpekerjaan)?"selected":"";?> ><?php echo $rpekerjaan->pekerjaan;?></option>
			<?php endforeach;?>
			</select>
		</td>
	</tr>
	<tr><td colspan="2" class="text-center"><button type="submit" class="btn btn-success">Simpan</button> || <a href="<?php echo base_url();?>admin" class="btn btn-warning">Back</a></td></tr>
</table>
</form>
