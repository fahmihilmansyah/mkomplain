<div class="panel panel-primary">
	<div class="panel-heading">
		Report Complaint
	</div>
	<div class="panel-body">
		<form method="POST">
			<input type="date" name="tglawal" value="<?php echo date("Y-m-d")?>" placeholder="yyyy-mm-dd">
			<input type="date" name="tglakhir" value="<?php echo date("Y-m-d")?>" placeholder="yyyy-mm-dd">
			<button type="submit">Cari</button>
		</form>
		<table class="table table-strepped">
			<thead>
				<tr>
					<th>Kategori</th>
					<th>Jumlah</th>
				</tr>
			</thead>
			<tbody>
			<?php
			$dats = '';
			if(!empty($tglawal) || !empty($tglakhir)){
				$dats = "/$tglawal/$tglakhir";
			} 
			foreach ($isi as $r):?>
				<tr>
					<td><?php echo $r->nm_kategori ?></td>
					<td><a href="<?php echo base_url()?>complaint/findcomplaint/<?php echo $r->id_kategori,$dats ?>"><?php echo number_format($r->total) ?></a></td>
				</tr>

			<?php endforeach; ?>

			</tbody>
		</table>
	</div>
</div>
<table class="table table-strepped">
			<tr>
				<td>
					Komplian yang Belum di tanggapi <a href="<?php echo base_url()?>complaint/complaintbelum"><button class="form-control btn btn-success">Submit</button></a>
				</td>	
			</tr>
			<tr>
				<td>
					Komplian yang Sudah di tanggapi <a href="<?php echo base_url()?>complaint/complaintsudah"><button class="form-control btn btn-success">Submit</button></a>
				</td>
			</tr>
			<tr>
				<td>
					Data Komplain <a href="<?php echo base_url()?>complaint/complaintsudahblm"><button class="form-control btn btn-success">Submit</button></a>
				</td>
			</tr>
		</table>