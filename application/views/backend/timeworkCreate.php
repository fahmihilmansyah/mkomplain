<div>
	<h2>Create Waktu Kerja</h2>
</div>
<form method="post" action="<?php echo base_url();?>admin/doCreateTimework">
<table class="table ">
		<tr>
			<td>Karyawan</td><td>
			<select name="id_karyawan" >
				<?php foreach($karyawan as $rkaryawan):
				?>
				<option value="<?php echo $rkaryawan->id?>"  ><?php echo $rkaryawan->name?></option>
				<?php endforeach; ?>
			</select>
			</td>
		</tr>
		<tr>
			<td>Senin</td><td>
				<div class="form-group">
					<input type="radio" name="senin" value="1" >Ada
					<input type="radio" name="senin" value="0"  checked>Tidak Ada
				</div>
			</td>
		</tr>
		<tr>
			<td>Selasa</td><td>
				<div class="form-group">
					<input type="radio" name="selasa" value="1" >Ada
					<input type="radio" name="selasa" value="0"  checked>Tidak Ada
				</div>
			</td>
		</tr>
		<tr>
			<td>Rabu</td><td>
				<div class="form-group">
					<input type="radio" name="rabu" value="1" >Ada
					<input type="radio" name="rabu" value="0"  checked>Tidak Ada
				</div>
			</td>
		</tr>
		<tr>
			<td>Kamis</td><td>
				<div class="form-group">
					<input type="radio" name="kamis" value="1">Ada
					<input type="radio" name="kamis" value="0" checked>Tidak Ada
				</div>
			</td>
		</tr>
		<tr>
			<td>Jumat</td><td>
				<div class="form-group">
					<input type="radio" name="jumat" value="1" >Ada
					<input type="radio" name="jumat" value="0" checked>Tidak Ada
				</div>
			</td>
		</tr>
		<tr>
			<td>Sabtu</td><td>
				<div class="form-group">
					<input type="radio" name="sabtu" value="1">Ada
					<input type="radio" name="sabtu" value="0" checked>Tidak Ada
				</div>
			</td>
		</tr>
		<tr>
			<td>Minggu</td><td>
				<div class="form-group">
					<input type="radio" name="minggu" value="1">Ada
					<input type="radio" name="minggu" value="0" checked>Tidak Ada
				</div>
			</td>
		</tr>
		<tr>
			<td>Jam Masuk</td><td><input type="time" name="jam_masuk" value="" placeholder="hh-mm-ss"></td>
		</tr>
		<tr>
			<td>Jam Keluar</td><td><input type="time" name="jam_keluar" value="" placeholder="hh-mm-ss"></td>
		</tr>
		<tr><td colspan="2" class="text-center"><button class="btn btn-success" type="submit">Simpan</button> || <a class="btn btn-danger" href="<?php echo base_url()?>admin/listTimework">Back</a></td></tr>
</table>