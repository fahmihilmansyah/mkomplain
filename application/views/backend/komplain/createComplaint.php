<h2>Create Complaint</h2>
<hr>
<form method='post' action="<?php echo base_url()?>komplain/doInsertKomplain">
        <div class="row">
          <div class="col-12">
              <form>
                  <div class="form-group">
                      <label for="exampleFormControlInput1">Cabang</label>
                      <select class="form-control select2" name="cabangid" id="">
                          <?php foreach ($cabang as $r): ?>
                          <option value="<?php echo $r->id?>"><?php echo $r->namaCabang?></option>
                          <?php endforeach;?>
                      </select>
                  </div>
                  <div class="form-group">
                      <label>Kategori</label>
                      <select class="form-control select2" name="kategoriid" id="">
                          <?php foreach ($kategori as $r): ?>
                              <option value="<?php echo $r->id_kategori?>"><?php echo $r->nm_kategori?></option>
                          <?php endforeach;?>
                      </select>
                  </div>
                  <div class="form-group">
                      <label>Complaint</label>
                      <textarea class="form-control" name="isikomplain" placeholder=""></textarea>
                  </div>
                  <div class="form-group">
                      <label>Prioritas:</label>
                      <select class="form-control" name="prio" >
                          <option value="Low Complaint">Low Complaint</option>
                          <option value="Medium Complaint">Medium Complaint</option>
                          <option value="High Complaint">High Complaint</option>
                      </select>
                  </div>

                  <div class="form-group float-right mt-2">
                      <button type="submit" class="btn btn-sm btn-success">Submit</button>
                      <button type="reset"  class="btn btn-sm btn-danger">Reset</button>
                  </div>
              </form>
          </div>
        </div>
      </form>
<script>
    $('.select2').select2();
</script>