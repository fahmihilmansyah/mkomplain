<div class="group-form">
	<h2>List Complaint</h2>
	<hr>
</div>
<div class="form-group">
<?php $hk = $this->session->userdata('hak_akses');
?>
	<a href="<?php echo base_url()?>komplain/createComplaint" class="btn btn-sm float-right btn-primary">Create Complaint</a>
</div>
<table class="table ">
	<thead>
		<tr>
			<th>Cabang</th>
			<th>Kriteria Komplain</th>
			<th>Isi Komplain</th>
			<th>Prioritas</th>
			<th>Tanggal Komplain</th>
			<th>Status Komplain</th>
			<th>Action</th>
		</tr>
	</thead>
	<tbody>
		<?php
		 foreach($isi as $row):
		 	?>
		<tr>
			<td><?php echo $row->namaCabang?></td>
			<td><?php echo $row->nm_kategori?></td>
			<td><?php echo $row->isi_komplain?></td>
			<td><?php echo $row->prioritas?></td>
			<td><?php echo $row->date_created?></td>
			<td><?php echo $row->active == 1 ? "<label class='btn btn-sm btn-success'>Active</label>": ($row->active == 2 ? "<a href='".base_url()."complaint/answareComplaintList/".$row->id."' class=' btn-sm btn btn-warning'>Replied</a>":"<label class='btn btn-sm btn-danger'>Cancel</label>")?></td>
			<td><?php if($hk != 4 ):?>
				<a href="<?php echo base_url();?>komplain/answareComplaint/<?php echo $row->id?>" class='btn btn-sm btn-primary'>Respond</a>
				<?php else:echo "<label class='btn btn-danger'>No-Action</label>"; endif; ?>
			</td>
		</tr>
	<?php
	endforeach; ?>
	</tbody>
</table>