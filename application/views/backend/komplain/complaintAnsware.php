<div class="group-form">
    <h2>Complaint</h2>
    <hr>
</div>
<table class="table table-striped">
    <thead>
    <tr>
        <th>Cabang</th>
        <th>Kriteria Komplain</th>
        <th>Isi Komplen</th>
        <th>Tanggal Komplen</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($isi as $row): ?>
        <tr>
            <td><?php echo $row->namaCabang ?></td>
            <td><?php echo $row->nm_kategori ?></td>
            <td><?php echo $row->isi_komplain ?></td>
            <td><?php echo $row->date_created ?></td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
<br>
<hr>
<div>
    <h2>Answare Complaint</h2>
</div>
<div class="container">
    <div class="row">
        <table class="table table-striped">
            <thead>
            <tr>
                <th>Case Link</th>
                <th>Root Cause</th>
                <th>Impact</th>
                <th>Action</th>
                <th>Down time</th>
                <th>Up time</th>
                <th>Tanggal Komplain</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($isi1 as $row1): ?>
                <tr>
                    <td><?php echo $row1->case_link ?></td>
                    <td><?php echo $row1->root_cause ?></td>
                    <td><?php echo $row1->impact ?></td>
                    <td><?php echo $row1->action ?></td>
                    <td><?php echo $row1->downtime ?></td>
                    <td><?php echo $row1->uptime ?></td>
                    <td><?php echo $row1->created_ad ?></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>
<br>
<hr>
<div class="container">
    <div class="row">
        <div class="form-group">
            <form action="<?php echo base_url() ?>komplain/doAnswareComplaint" method="post">
                <div class="row">
                <div class="form-group col-12">
                    <label>Karyawan: </label>&nbsp;<label><?php echo $this->session->userdata('name'); ?></label>
                    <input type="hidden" name="id_karyawan"
                           value="<?php echo $this->session->userdata('id_karyawan'); ?>">
                    <input type="hidden" name="id_komplain" value="<?php echo $isi[0]->id ?>">
                </div>
                <div class="form-group col-6">
                    <label for="case_link">Case Link:</label>
                    <input type="text" id="case_link" name="case_link" class="form-control"
                              placeholder="Masukkin Case Link">
                </div>
                <div class="form-group col-6">
                    <label for="root_cause">Root Cause:</label>
                    <input type="text" id="root_cause" name="root_cause" class="form-control"
                              placeholder="Masukkin Root Cause">
                </div>
                <div class="form-group col-6">
                    <label for="impact">Impact:</label>
                    <input type="text" id="impact" name="impact" class="form-control"
                              placeholder="Masukkin Impact">
                </div>
                <div class="form-group col-6">
                    <label for="action">Action:</label>
                    <input type="text" id="action" name="action" class="form-control"
                              placeholder="Masukkin Action">
                </div>
                <div class="form-group col-6">
                    <label for="down_time">Down Time:</label>
                    <input type="time" id="down_time" name="down_time" class="form-control"
                              placeholder="Masukkin Down Time">
                </div>
                <div class="form-group col-6">
                    <label for="down_time">Up Time:</label>
                    <input type="time" id="up_time" name="up_time" class="form-control"
                              placeholder="Masukkin Up Time">
                </div>
                    <div class="form-group mt-2">
                    <button class="btn-sm float-right btn btn-success">Simpan</button>
                </div>
                </div>
            </form>
        </div>
    </div>
</div>