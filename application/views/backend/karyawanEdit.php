<div class="form-group">
	<h2>Edit Karyawan</h2>
</div>
<form method="post" action="<?php echo base_url()?>admin/doEditKaryawan/<?php echo $qry[0]->id?>">
<table class="table">
	<tr>		
		<td>Nama</td><td><input type="text" name="name" value="<?php echo $qry[0]->name ?>" placeholder="Nama"></td>
	</tr>
	<tr>		
		<td>Jabatan</td><td>
		<select name="nama_jabatan" >
			<?php foreach ($jabatan as $rjabatan):?>
			<option value="<?php echo $rjabatan->id_jabatan?>" <?php echo ($rjabatan->id_jabatan == $qry[0]->id_jabatan)?"selected":"";?> ><?php echo $rjabatan->nama_jabatan?></option>
		<?php endforeach;?>
		</select>
		</td>
	</tr>
	<tr>		
		<td>KTP / SIM</td><td><input type="text" name="ktp" value="<?php echo $qry[0]->ktp ?>" placeholder="Masukan No Ktp"></td>
	</tr>
	<tr>		
		<td>Alamat</td><td><textarea name="address"><?php echo $qry[0]->address ?></textarea></td>
	</tr>
	<tr>		
		<td>Alamat2</td><td><textarea name="address2"><?php echo $qry[0]->address2 ?></textarea></td>
	</tr>
	<tr>		
		<td>Tempat Tgl Lahir</td><td><input type="text" name="tempt_lahir" value="<?php echo $qry[0]->tempt_lahir ?>" placeholder="Tempat Lahir">||<input type="date" name="tgl_lahir" value="<?php echo $qry[0]->tgl_lahir ?>" placeholder="yyyy-mm-dd"></td>
	</tr>
	<tr>		
		<td>No. Telp</td><td><input type="text" name="no_telp" value="<?php echo $qry[0]->no_telp ?>" placeholder="Telp"></td>
	</tr>
	<tr>		
		<td>No. HP</td><td><input type="text" name="no_hp" value="<?php echo $qry[0]->no_hp ?>" placeholder=""></td>
	</tr>
	<tr>
		<td colspan="2" class="text-center"><button class="btn btn-primary" type="submit">Simpan</button> || <a href="<?php echo base_url()?>admin" class="btn btn-danger">Batal</a></td>
	</tr>
</table>
</form>