<div class="form-group">
	<h2>List Karyawan</h2>
	<hr>
	<a href="<?php echo base_url()?>admin/createKaryawan" class="btn btn-sm btn-warning"><i class="icofont icofont-plus-circle"></i> Karyawan</a> <a href="<?php echo base_url()?>login/listLogin" class="btn btn-sm btn-primary"><i class="icofont icofont-sign-in"></i> Login Karyawan</a>
</div>
<table class="table">
	<thead>
		<tr>
			<th class="text-center">Nama Member</th>
			<th class="text-center">Jabatan</th>
			<th class="text-center">KTP/SIM</th>
			<th class="text-center">Tempat Lahir</th>
			<th class="text-center">Tanggal Lahir</th>
			<th class="text-center">Alamat</th>
			<th class="text-center">Alamat 2</th>
			<th class="text-center">Action</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($isi as $row):?>
		<tr>
			<td><?php echo $row->name?></td>
			<td><?php echo $row->nama_jabatan?></td>
			<td><?php echo $row->ktp?></td>
			<td><?php echo $row->tempt_lahir?></td>
			<td width="120"><?php echo date('d-m-Y',strtotime($row->tgl_lahir))?></td>
			<td><?php echo $row->address?></td>
			<td><?php echo $row->address2?></td>
            <td class="text-center" width="200"> <a href="<?php echo base_url() ?>admin/getKaryawanId/<?php echo $row->id?>" class="btn btn-sm btn-primary"><i class="icofont icofont-eye-alt"></i></a> <a href="<?php echo base_url() ?>admin/editKaryawan/<?php echo $row->id?>" class="btn btn-sm btn-success"><i class="icofont icofont-edit-alt"></i></a> <a href="<?php echo base_url() ?>admin/deleteKaryawan/<?php echo $row->id?>" class="btn btn-sm btn-danger"><i class="icofont icofont-delete-alt"></i></a></td>
		</tr>
	<?php endforeach; ?>
	</tbody>
</table>
