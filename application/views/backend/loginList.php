<div>
	<h2>List Login</h2>
</div>
<a class="btn btn-primary" href="<?php echo base_url()?>login/createLogin">Create Login User</a>
<hr>
<table class="table">
	<thead>
		<tr>
			<th>Nama Karyawan</th>
			<th>Username</th>
			<th>Hak Akses</th>
			<th>Action</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($isi as $r):?>
		<tr>
			<td><?php echo $r->name?></td>
			<td><?php echo $r->username?></td>
			<td><?php echo $r->hak_akses == 2 ?"Admin":($r->hak_akses == 1 ? "SPV": "Manager")?></td>
			<td><a href="<?php echo base_url()?>login/editLogin/<?php echo $r->id?>"  class="btn btn-success">Edit</a> || <a href="<?php echo base_url();?>login/doDelete/<?php echo $r->id?>" class="btn btn-danger"> Delete</a></td>
		</tr>
	<?php endforeach;?>
	</tbody>
</table>