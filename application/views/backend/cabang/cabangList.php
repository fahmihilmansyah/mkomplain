<div class="form-group">
	<h2>List Cabang</h2>
	<hr>
	<a href="<?php echo base_url()?>cabang/createMember" class="float-right btn btn-warning">Create Cabang</a>
</div>
<div>
<?php 
$msg = $this->session->flashdata('message');
if(isset($msg) && !empty($msg)):
echo "<script>alert('".$msg."');</script>";
endif;
?>
</div>
<table class="table">
	<thead>
		<tr>
			<th class="text-center">Nama Cabang</th>
			<th class="text-center">Alamat Cabang</th>
			<th class="text-center">No Telp</th>
			<th class="text-center">Email</th>
			<th class="text-center">Action</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($isi as $row):?>
		<tr>
			<td><?php echo $row->namaCabang?></td>
			<td><?php echo $row->alamat?></td>
			<td><?php echo $row->notelp?></td>
			<td><?php echo $row->email?></td>
            <td class="text-center"> <label data-href="<?php echo base_url() ?>cabang/getMember/<?php echo $row->id?>" class="smodal btn btn-sm btn-primary"><i class="icofont icofont-eye-alt"></i></label> <a href="<?php echo base_url() ?>cabang/editMember/<?php echo $row->id?>" class="btn btn-sm btn-success"><i class="icofont icofont-edit-alt"></i></a> </td>
		</tr>
	<?php endforeach; ?>
	</tbody>
</table>
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">&nbsp;</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
            </div>
        </div>
    </div>
</div>
<script>
    $('.smodal').on('click',function (){
        var url = $(this).attr('data-href');
        $('#exampleModal').modal().find('.modal-body').load(url);
    })
</script>