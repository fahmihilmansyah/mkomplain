<div class="group-form">
	<h2>Komplain / Keluhan / Saran</h2>
	<hr>
</div>
<table class="table ">
	<thead>
		<tr>
			<th>Kode Member</th>
			<th>Nama Member</th>
			<th>Kriteria Komplain</th>
			<th>Isi Komplen</th>
			<th>Tanggal Komplain</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach($isi as $row):?>
		<tr>
			<td><?php echo $row->kode_member?></td>
			<td><?php echo $row->nama?></td>
			<td><?php echo $row->nm_kategori?></td>
			<td><?php echo $row->isi_komplain?></td>
			<td><?php echo $row->date_created?></td>
		</tr>
	<?php endforeach; ?>
	</tbody>
</table>
<br>
<hr>
<div>
	<h2>Jawaban Komplain</h2>
</div>
<div class="container">
	<div class="row">
		<table class="table ">
			<thead>
				<tr>
					<th>Karyawan</th>
					<th>Member</th>
					<th>Jawaban Komplain</th>
					<th>Tanggal Komplain</th>
				</tr>
			</thead>
			<tbody>
			<?php foreach($isi1 as $row1):?>
				<tr>
					<td><?php echo $row1->name?></td>
					<td><?php echo $row1->nama?></td>
					<td><?php echo $row1->isi_tanggapan?></td>
					<td><?php echo $row1->date_created?></td>
				</tr>
			<?php endforeach; ?>
			
			</tbody>
		</table>
	</div>
</div>
<hr>
<div class="container">
	<div class="row">
		<div class="form-group">
		<form action="<?php echo base_url()?>member/doAnswareComplaint" method="post">
			<input type="hidden" name="member_id" value="<?php echo $isi[0]->id_member;?>">
			<br>
			<input type="hidden" name="id_komplain" value="<?php echo $isi[0]->id?>" >
			<label for="tanggapan">Tanggapan Komplain:</label>
			<textarea id="tanggapan" name="isi_tanggapan" class="form-control" placeholder="Masukkin Tanggapan"></textarea>
			<button class="form-control btn btn-success">Simpan</button>
		</form>
		</div>
	</div>
</div>