<div class="form-group">
	<h2>Detail Member</h2>
</div>
<div class="form-group ">
	<a href="<?php echo base_url(); ?>admin/editMember/<?php echo $qry[0]->id_member ?>" class="btn btn-success">Edit Data</a>
</div>
<table class="table">
	<tr>
		<td>Kode Member</td><td><?php echo $qry[0]->kode_member ?></td>
	</tr>
	<tr>		
		<td>Nama</td><td><?php echo $qry[0]->nama ?></td>
	</tr>
	<tr>		
		<td>KTP / SIM</td><td><?php echo $qry[0]->no_ktp_sim ?></td>
	</tr>
	<tr>		
		<td>Alamat</td><td><?php echo $qry[0]->alamat ?></td>
	</tr>
	<tr>		
		<td>Email</td><td><?php echo $qry[0]->email ?></td>
	</tr>
	<tr>		
		<td>Tempat Tgl Lahir</td><td><?php echo $qry[0]->lahir.", ".$qry[0]->tanggal_lahir ?></td>
	</tr>
	<tr>		
		<td>Ahli Waris</td><td><?php echo $qry[0]->ahli_waris ?></td>
	</tr>
	<tr>		
		<td>Agama</td><td><?php echo $qry[0]->agama ?></td>
	</tr>
	<tr>		
		<td>No. Telp</td><td><?php echo $qry[0]->no_tlp ?></td>
	</tr>
	<tr>		
		<td>Status Perkawinan</td><td><?php echo $qry[0]->statusPerkawinan ?></td>
	</tr>
	<tr>		
		<td>Jenis Kelamin</td><td><?php echo $qry[0]->gender ?></td>
	</tr>
	<tr>		
		<td>Pendidikan</td><td><?php echo $qry[0]->pendidikan ?></td>
	</tr>
	<tr>		
		<td>Pekerjaan</td><td><?php echo $qry[0]->pekerjaan ?></td>
	</tr>
</table>
