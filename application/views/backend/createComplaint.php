<h2>Create Complaint</h2>
<hr>
<form method='post' action="<?php echo base_url()?>member/doInsertKomplain">
        <div class="row" >
        <div class="form-group">
          <label for="kdMember">Kode Member</label><input autocomplete="off" class="form-control" type="text" name="kdMember" id="kdMember" value="" placeholder="Please Input Your Member Code">
          <label id="chkkd" class="btn btn-warning">Check</label>
        </div>
        </div>
        <hr>
        <div class="row">
          <div class="result">
            <div class="form-group">
                <div class="rexs"></div>
              <label for="">Name:</label> <label><span class="nma"></span></label><br>
              <label for="nmEmail">Email:</label><input class="form-control" type="email" name="nmEmail" readonly id="nmEmail" value="" placeholder="Please Input Your Email">
              <label for="kdKat">Kategori</label>
                <select class="form-control" name="kdKat" id="kdKat">
                  <option value="0">--Pilih Kategori--</option>
                  <?php foreach($isi as $row): ?>
                    <option value="<?php echo $row->id_kategori ?>"><?php echo $row->nm_kategori ?></option>
                  <?php endforeach; ?>
                </select>
              <label for="nmComp">Complaint:</label><textarea class="form-control" name="nmComp" id="nmComp" value="" placeholder="Please Input Your Complaint"></textarea><br>
              <LABEL>Prioritas:</LABEL>
              <select class="form-control" name="prio" >
                <option value="Low Complaint">Low Complaint</option>
                <option value="Medium Complaint">Medium Complaint</option>
                <option value="High Complaint">High Complaint</option>
                
              </select><br>
              <button type="submit" class="btn btn-success">Submit Complaint</button>
              <button type="reset" class="btn btn-danger">Cancel</button>
            </div>
          </div>
        </div>
      </form>