<div class="group-form">
	<h2>Jawaban Komplain</h2>
	<hr>
</div>
<table class="table ">
	<thead>
		<tr>
			<th>Kode Member</th>
			<th>Nama Member</th>
			<th>Kriteria Komplen</th>
			<th>Isi Komplain</th>
			<th>Tanggal Komplain</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach($isi as $row):?>
		<tr>
			<td><?php echo $row->kode_member?></td>
			<td><?php echo $row->nama?></td>
			<td><?php echo $row->nm_kategori?></td>
			<td><?php echo $row->isi_komplain?></td>
			<td><?php echo $row->date_created?></td>
		</tr>
	<?php endforeach; ?>
	</tbody>
</table>
<br>
<hr>
<div>
	<h2>Jawaban</h2>
</div>
<div class="container">
	<div class="row">
		<table class="table ">
			<thead>
				<tr>
					<th>Karyawan</th>
					<th>Member</th>
					<th>Jawaban Komplain</th>
					<th>Tanggal jawaban Komplain</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach($isi1 as $row1):?>
				<tr>
					<td><?php echo $row1->name?></td>
					<td><?php echo $row1->nama?></td>
					<td><?php echo $row1->isi_tanggapan?></td>
					<td><?php echo $row1->date_created?></td>
				</tr>
			<?php endforeach; ?>
			</tbody>
		</table>
	</div>
</div>