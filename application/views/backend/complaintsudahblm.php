<div class="group-form">
	<h2>Isi Komplain</h2>
	<hr>
</div>
<div class="form-group">
<?php $hk = $this->session->userdata('hak_akses');
if( $hk == 4):
?>
	<a href="<?php echo base_url()?>complaint/createComplaint" class="btn btn-primary">Create Complaint</a>
<?php endif?>
</div>
<table class="table ">
	<thead>
		<tr>
			<th>Kode Member</th>
			<th>Nama Member</th>
			<th>Kriteria Komplain</th>
			<th>Isi Komplain</th>
			<th>Prioritas</th>
			<th>Tanggal Komplain</th>
			<th>Status Komplain</th>
			<th>Action</th>
		</tr>
	</thead>
	<tbody>
		<?php
		 foreach($isi as $row):?>
		<tr>
			<td><?php echo $row->kode_member?></td>
			<td><?php echo $row->nama?></td>
			<td><?php echo $row->nm_kategori?></td>
			<td><?php echo $row->isi_komplain?></td>
			<td><?php echo $row->prioritas?></td>
			<td><?php echo $row->date_created?></td>
			<td><?php echo $row->active == 1 ? "<label class='btn btn-success'>Active</label>": ($row->active == 2 ? "<a href='".base_url()."complaint/answareComplaintList/".$row->id."' class='btn btn-warning'>Replied</a>":"<label class='btn btn-danger'>Cancel</label>")?></td>
			<td><?php if($hk != 4 ):?>
				<a href="<?php echo base_url();?>complaint/answareComplaint/<?php echo $row->id?>" class='btn btn-primary'>Respond</a>
				<?php else:echo "<label class='btn btn-danger'>No-Action</label>"; endif; ?>
			</td>
		</tr>
	<?php 	endforeach; ?>
	</tbody>
</table>