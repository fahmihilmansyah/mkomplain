<div class="form-group">
	<h2>Create Karyawan</h2>
</div>
<form method="post" action="<?php echo base_url()?>admin/doCreateKaryawan">
<table class="table">
	<tr>		
		<td>Nama</td><td><input type="text" name="name" value="" placeholder="Nama"></td>
	</tr>
	<tr>		
		<td>Jabatan</td><td>
		<select name="nama_jabatan" >
			<?php foreach ($jabatan as $rjabatan):?>
			<option value="<?php echo $rjabatan->id_jabatan?>" ><?php echo $rjabatan->nama_jabatan?></option>
		<?php endforeach;?>
		</select>
		</td>
	</tr>
	<tr>		
		<td>KTP / SIM</td><td><input type="text" name="ktp" value="" placeholder="Masukan No Ktp"></td>
	</tr>
	<tr>		
		<td>Alamat</td><td><textarea name="address"></textarea></td>
	</tr>
	<tr>		
		<td>Alamat2</td><td><textarea name="address2"></textarea></td>
	</tr>
	<tr>		
		<td>Tempat Tgl Lahir</td><td><input type="text" name="tempt_lahir" value="" placeholder="Tempat Lahir">||<input type="date" name="tgl_lahir" value="" placeholder="yyyy-mm-dd"></td>
	</tr>
	<tr>		
		<td>No. Telp</td><td><input type="text" name="no_telp" value="" placeholder="Telp"></td>
	</tr>
	<tr>		
		<td>No. HP</td><td><input type="text" name="no_hp" value="" placeholder="No HP"></td>
	</tr>
	<tr>
		<td colspan="2" class="text-center"><button class="btn btn-primary" type="submit">Simpan</button> ||<button type="reset" class="btn btn-success">Reset</button>|| <a href="<?php echo base_url()?>admin" class="btn btn-danger">Batal</a></td>
	</tr>
</table>
</form>