<div class="form-group">
	<h2>Detail Karyawan</h2>
</div>
<div class="form-group ">
	<a href="<?php echo base_url(); ?>admin/editKaryawan/<?php echo $qry[0]->id ?>" class="btn btn-success">Edit Data</a>
</div>
<table class="table">
	<tr>		
		<td>Nama</td><td><?php echo $qry[0]->name ?></td>
	</tr>
	<tr>		
		<td>Jabatan</td><td><?php echo $qry[0]->nama_jabatan ?></td>
	</tr>
	<tr>		
		<td>KTP / SIM</td><td><?php echo $qry[0]->ktp ?></td>
	</tr>
	<tr>		
		<td>Alamat</td><td><?php echo $qry[0]->address ?></td>
	</tr>
	<tr>		
		<td>Alamat</td><td><?php echo $qry[0]->address2 ?></td>
	</tr>
	<tr>		
		<td>Tempat Tgl Lahir</td><td><?php echo $qry[0]->tempt_lahir.", ".date("d-m-Y",strtotime($qry[0]->tgl_lahir)) ?></td>
	</tr>
	<tr>		
		<td>No. Telp</td><td><?php echo $qry[0]->no_telp ?></td>
	</tr>
	<tr>		
		<td>No. Telp</td><td><?php echo $qry[0]->no_hp ?></td>
	</tr>
	
</table>
<div class="form-group">
	<h2>Waktu Kerja</h2>
	<hr>	
</div>
<table class="table">
	<thead>
		<tr>
			<th>SENIN</th>
			<th>SELASA</th>
			<th>RABU</th>
			<th>KAMIS</th>
			<th>JUMAT</th>
			<th>SABTU</th>
			<th>MINGGU</th>
			<th>Jam Masuk</th>
			<th>Jam Keluar</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td><?php echo ($qry[0]->senin == 1?"<label class='btn btn-primary'>ADA</label>":"<label class='btn btn-danger'>KOSONG</label>")?></td>
			<td><?php echo ($qry[0]->selasa == 1?"<label class='btn btn-primary'>ADA</label>":"<label class='btn btn-danger'>KOSONG</label>")?></td>
			<td><?php echo ($qry[0]->rabu == 1?"<label class='btn btn-primary'>ADA</label>":"<label class='btn btn-danger'>KOSONG</label>")?></td>
			<td><?php echo ($qry[0]->kamis == 1?"<label class='btn btn-primary'>ADA</label>":"<label class='btn btn-danger'>KOSONG</label>")?></td>
			<td><?php echo ($qry[0]->jumat == 1?"<label class='btn btn-primary'>ADA</label>":"<label class='btn btn-danger'>KOSONG</label>")?></td>
			<td><?php echo ($qry[0]->sabtu == 1?"<label class='btn btn-primary'>ADA</label>":"<label class='btn btn-danger'>KOSONG</label>")?></td>
			<td><?php echo ($qry[0]->minggu == 1?"<label class='btn btn-primary'>ADA</label>":"<label class='btn btn-danger'>KOSONG</label>")?></td>
			<td><?php echo ($qry[0]->jam_masuk)?></td>
			<td><?php echo ($qry[0]->jam_keluar)?></td>
		</tr>
	</tbody>
</table>