<div class="form-group">
	<h2>List Member</h2>
	<hr>
	<a href="<?php echo base_url()?>admin/createMember" class="btn btn-warning">Create Member</a>
</div>
<div>
<?php 
$msg = $this->session->flashdata('message');
if(isset($msg) && !empty($msg)):
echo "<script>alert('".$msg."');</script>";
endif;
?>
</div>
<table class="table">
	<thead>
		<tr>
			<th class="text-center">Kode Member</th>
			<th class="text-center">Nama Member</th>
			<th class="text-center">KTP/SIM</th>
			<th class="text-center">Alamat</th>
			<th class="text-center">Lahir</th>
			<th class="text-center">Tanggal Lahir</th>
			<th class="text-center">Action</th>
			
		</tr>
	</thead>
	<tbody>
		<?php foreach ($isi as $row):?>
		<tr>
			<td><?php echo $row->kode_member?></td>
			<td><?php echo $row->nama?></td>
			<td><?php echo $row->no_ktp_sim?></td>
			<td><?php echo $row->alamat?></td>
			<td><?php echo $row->lahir?></td>
			<td><?php echo date("d-m-Y",strtotime($row->tanggal_lahir))?></td>
			<td class="text-center"> <a href="<?php echo base_url() ?>admin/getMember/<?php echo $row->id_member?>" class="btn btn-primary">View</a> || <a href="<?php echo base_url() ?>admin/editMember/<?php echo $row->id_member?>" class="btn btn-success">Edit</a> </td>
		</tr>
	<?php endforeach; ?>
	</tbody>
</table>
