<table class="table ">
	<thead>
		<tr>
			<th>Kode Member</th>
			<th>Nama Member</th>
			<th>Kriteria Komplain</th>
			<th>Isi Komplain</th>
			<th>Prioritas</th>
			<th>Tanggal Komplain</th>
			<th>Status Komplain</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach($isi as $row):?>
		<tr>
			<td><?php echo $row->kode_member?></td>
			<td><?php echo $row->nama?></td>
			<td><?php echo $row->nm_kategori?></td>
			<td><?php echo $row->isi_komplain?></td>
			<td><?php echo $row->prioritas?></td>
			<td><?php echo $row->date_created?></td>
			<td><?php echo $row->active == 1 ? "<label class='btn btn-success'>Active</label>": ($row->active == 2 ? "<a href='".base_url()."complaint/answareComplaintList/".$row->id."' class='btn btn-warning'>Replied</a>":"<label class='btn btn-danger'>Cancel</label>")?></td>
		</tr>
	<?php endforeach; ?>
	</tbody>
</table>