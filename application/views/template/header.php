<?php $hk = $this->session->userdata('hak_akses');
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>GRIYA HEMAT</title>
<!--    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">-->
<!--    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>-->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/bootstrap.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/icofont/icofont.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/select2/css/select2.css">
<!--    <script src="--><?php //echo base_url(); ?><!--assets/dist/js/bootstrap.js" type="text/javascript" ></script>-->
    <!-- JS, Popper.js, and jQuery -->
<!--    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>-->
    <script src="<?php echo base_url(); ?>assets/js/jquery3.5.js" type="text/javascript" ></script>
    <script src="<?php echo base_url(); ?>assets/select2/js/select2.js" type="text/javascript" ></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
    <script type="text/javascript">
  $(document).ready(function(){
    $(".hideme").hide();
    var urlist = "<?php echo base_url()?>";
    jQuery(document).on('click',"#chkkd",function(){
      $.ajax({
            url: urlist+"member/checkMember",
            type :'POST',
            data : {
                    kode_member: $('#kdMember').val(),
                  },
            dataType : 'json',
            cache : false,
            beforeSend:function(){
              $('.rexs').empty();
            },
            success:function(msg){              
                if(msg.result ){
                  $('.nma').html(msg.isi.nama);
                  console.log(msg.isi.idmember);
                  $('.rexs').append("<input type='hidden' name='idmember' id='idmembers' value='"+msg.isi.idmember+"'>");
                  $('#nmEmail').val(msg.isi.email);
                  getComplaint(msg.isi.idmember);
                  $('.rescomplaint').show();
                }else{
                  alert("false");
                }
              }
          });
    });
  });
  </script>
</head>
<body>
	<div class="container">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="#">GRIYA HEMAT SOEKARNO-HATTA</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav">
                    <li class="nav-item active">
                        <a class="nav-link" href="<?php echo base_url()?>admin">Home <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo base_url()?>cabang/listMember">Cabang</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo base_url()?>admin/listKaryawan">Karyawan</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo base_url()?>admin/listTimework">Waktu Kerja</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo base_url()?>komplain/lapComplaint">Report komplain</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo base_url()?>komplain/listComplaint">komplain</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo base_url()?>login/doDestroy">Logout</a>
                    </li>
                </ul>
            </div>
        </nav>