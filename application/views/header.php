<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>GRIYA HEMAT</title>
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.css">
  <script src="<?php echo base_url(); ?>assets/js/bootstrap.js" type="text/javascript" ></script>
  <script src="<?php echo base_url(); ?>assets/js/jquery.js" type="text/javascript" ></script>
  <script src="<?php echo base_url(); ?>assets/js/dropdown.js" type="text/javascript" ></script>
  <script src="<?php echo base_url(); ?>assets/js/modal.js" type="text/javascript" ></script>
	<script src="<?php echo base_url(); ?>assets/js/tab.js" type="text/javascript" ></script>
  <script type="text/javascript">
  $(document).ready(function(){
    var urlist = "<?php echo base_url()?>";
    $('.rescomplaint').hide();
    $('.rescomplaintT').hide();
    $('#myTabs a').click(function (e) {
      e.preventDefault()
      $(this).tab('show')
    });
    $("#chkkd").click(function(){
      $.ajax({
            url: urlist+"member/checkMember",
            type :'POST',
            data : {
                    kode_member: $('#kdMember').val(),
                  },
            dataType : 'json',
            cache : false,
            beforeSend:function(){
              $('.rexs').empty();
            },
            success:function(msg){              
                if(msg.result ){
                  $('.nma').html(msg.isi.nama);
                  console.log(msg.isi.idmember);
                  $('.rexs').append("<input type='hidden' name='idmember' id='idmembers' value='"+msg.isi.idmember+"'>");
                  $('#nmEmail').val(msg.isi.email);
                  getComplaint(msg.isi.idmember);
                  $('.rescomplaint').show();
                }else{
                  alert("Maaf Kode Member Tidak Ditemukan");
                }
              }
          });
    });
    function getComplaint(id){
      $.ajax({
            url: urlist+"member/checkKomplain",
            type :'POST',
            data : {
                    kode_member: id,
                  },
            dataType : 'json',
            beforeSend:function(){
              $(".complaintdatas").empty();
            },
            success:function(msg){   
            var isi ="";           
            var actived="";         
                  $.each(msg.isi, function(){
                    console.log(this.nama);
                    console.log(this.isi_komplain);
                    if(this.active == 1){
                      actived = "<label class='btn btn-success'>Active</label>";
                    }
                    if(this.active == 2){
                      actived = "<a href='"+urlist+"member/getComplaintDetail/"+this.id_komplain+"/"+id+"' class='repl btn btn-warning' data-idkom='"+this.id_komplain+"'  >Replied</a> ";
                    }
                    isi += "<tr>"+
                    "<td>"+this.id_komplain+"</td>"+
                    "<td>"+this.nm_kategori+"</td>"+
                    "<td>"+this.isi_komplain+"</td>"+
                    "<td>"+this.date_created+"</td>"+
                    "<td>"+actived+"</td>"+
                    "</tr>";
                  });
                    $(".complaintdatas").append(isi);
              }
          });
    }
    
    
  });
  </script>
</head>
<body>
  <div class="container">
       <div class="header clearfix">
        <nav>
          <ul class="nav nav-pills pull-right">
            <li role="presentation" class="active"><a href="<?php echo base_url() ?>">Home</a></li>
            
          </ul>
        </nav>
        <h3 class="text-muted">GRIYA HEMAT</h3>
      </div>

    <!-- Main jumbotron for a primary marketing message or call to action -->
   