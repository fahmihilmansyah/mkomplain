-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 08, 2016 at 09:59 PM
-- Server version: 5.5.47-MariaDB-1~trusty-log
-- PHP Version: 5.6.17-1+deb.sury.org~trusty+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db_retail`
--

-- --------------------------------------------------------

--
-- Table structure for table `agama`
--

CREATE TABLE IF NOT EXISTS `agama` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `agama` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `agama`
--

INSERT INTO `agama` (`id`, `agama`) VALUES
(1, 'ISLAM'),
(2, 'KRISTEN'),
(3, 'PROTESTAN'),
(4, 'HINDU'),
(5, 'BUDHA'),
(6, 'KONGHUCU');

-- --------------------------------------------------------

--
-- Table structure for table `gender`
--

CREATE TABLE IF NOT EXISTS `gender` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gender` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `gender`
--

INSERT INTO `gender` (`id`, `gender`) VALUES
(1, 'LAKI-LAKI'),
(2, 'PEREMPUAN'),
(3, 'LAIN-LAIN');

-- --------------------------------------------------------

--
-- Table structure for table `karyawan`
--

CREATE TABLE IF NOT EXISTS `karyawan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_jabatan` int(11) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `ktp` varchar(100) DEFAULT NULL,
  `tempt_lahir` varchar(100) DEFAULT NULL,
  `tgl_lahir` date DEFAULT NULL,
  `no_telp` varchar(30) DEFAULT NULL,
  `no_hp` varchar(30) DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  `address2` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='\r\n' AUTO_INCREMENT=12 ;

--
-- Dumping data for table `karyawan`
--

INSERT INTO `karyawan` (`id`, `id_jabatan`, `name`, `ktp`, `tempt_lahir`, `tgl_lahir`, `no_telp`, `no_hp`, `address`, `address2`) VALUES
(1, 9, 'Saepunajat', '32021702860061', 'Sukabumi', '1986-02-17', '-', '085659408070', 'Jl. Buah Batu No.23', 'jalan R.A Kosasih no.29 Sukabumi'),
(2, 1, 'victor Djohartono', '3272087858343', 'Bandung', '1976-09-23', '08127657652', '022-7778768', 'jalan pasteur no.324 Bandung', 'jalan setra sari komplek setra sari B no.12'),
(3, 2, 'Idvan Krisnardi', '312312876874', 'Bandung', '1984-07-23', '02276546546', '08785456125', 'jalan sudirman no.121 Bandung', ''),
(4, 8, 'Febrina Siska', '3123776848783', 'Ciamis', '1986-01-23', '0227678839', '08137657629', 'jalan Dago No.231 Bandung', 'Taman Kopo Indah Block C4 No.21 Bandung'),
(5, 5, 'Agustina', '31765898798723', 'Bandung', '1982-04-12', '0228673653', '08784566273', 'Kopo Permai block D No.12 Bandung', 'Taman Kopo Indah Blok G No.13 Bandung'),
(6, 7, 'Dewi Riyanti', '3123123767844', 'Bandung', '1980-03-09', '02276876873', '081264765723', 'Jalan Kiara Condong No.234 Bandung', ''),
(7, 10, 'Papo Poernama', '31767263764872', 'Sumedang', '1972-01-25', '0228767383', '081276576573', 'Cimahi Permai No.23 Bandung', 'Jalan Jatinangor No.657 Sumedang'),
(8, 4, 'Utep Wahyudin', '3287687683432', 'Bandung', '1979-07-12', '02276576573', '0856567576572', 'JLN. RAYA BANJARAN 148\r\nKMP. KAUM UTARA RT 01/RW 03', ''),
(9, 3, 'Aida Fitriani', '312234341231', 'Tasikmalaya', '1975-06-06', '022768767987', '081378768723', 'JL.BATUNUNGGAL INDAH I NO.154\r\nBANDUNG', 'JL. SUNDA NO 56 \r\nBANDUNG'),
(11, 11, 'Fahmi', '1231287468216', 'Serang', '1992-06-26', '11516514654', '51561651', 'Buah batu no.12 Cijagra Bandung', 'jalan bahureksa no.43 jakarta');

-- --------------------------------------------------------

--
-- Table structure for table `komplain`
--

CREATE TABLE IF NOT EXISTS `komplain` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_number` int(11) DEFAULT NULL,
  `kategori_number` int(11) DEFAULT NULL,
  `isi_komplain` text,
  `prioritas` varchar(25) NOT NULL DEFAULT 'Low Complaint',
  `created_by` varchar(10) NOT NULL DEFAULT 'SYSTEM',
  `date_created` datetime DEFAULT NULL,
  `active` int(11) DEFAULT '1' COMMENT '2=replied,1 = active. 0 = tidak',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `komplain`
--

INSERT INTO `komplain` (`id`, `member_number`, `kategori_number`, `isi_komplain`, `prioritas`, `created_by`, `date_created`, `active`) VALUES
(1, 2, 29, 'kapan ada harga promo lagi?', 'Low Complaint', 'admin', '2015-12-11 16:25:41', 2),
(2, 4, 33, 'kalo harga promo kenapa banyak yang kosong??', 'Low Complaint', 'admin', '2015-12-12 03:07:23', 1),
(3, 3, 28, 'Kasirnya kurang menyapa dan jutek?', 'Low Complaint', 'admin', '2015-12-12 03:08:50', 2),
(4, 3, 34, 'kemaren saya beli baju ukuran L tapi kekecilan, bisia di tuker lagi ga?', 'Low Complaint', 'admin', '2015-12-12 03:09:48', 1),
(5, 6, 35, 'kapan ada parkir ada gratis?', 'Low Complaint', 'admin', '2015-12-12 03:11:03', 2);

-- --------------------------------------------------------

--
-- Table structure for table `komplainTanggapan`
--

CREATE TABLE IF NOT EXISTS `komplainTanggapan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_komplain` int(11) DEFAULT '0',
  `id_karyawan` int(11) DEFAULT '0',
  `member_id` int(11) DEFAULT '0',
  `isi_tanggapan` text,
  `date_created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_member` (`member_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `komplainTanggapan`
--

INSERT INTO `komplainTanggapan` (`id`, `id_komplain`, `id_karyawan`, `member_id`, `isi_tanggapan`, `date_created`) VALUES
(1, 1, 3, 0, 'untuk harga promo kami biasanya ada harga heran, promonya di mulai setiap  hari jumat sampai hari minggu, dan serba hemat tiap minggunya.', '2015-12-12 03:17:59'),
(2, 5, 3, 0, 'terima kasih atas masukannya, untuk parkir gratis akan kita tampung masukannya dan nanti akan kita coba evaluasi dan menerapkanya\r\nTerima kasih.', '2015-12-12 03:19:46'),
(3, 3, 5, 0, 'Sebelumnya kami mohon maaf apabila, kasir kami kurang sopan dalam pelayanannya, kami akan mengevaluasi kasir di cabang kami agar menerapkan kembali Senyum, Sapa dan Salam.', '2015-12-12 03:22:24'),
(5, 1, 0, 2, 'asdasdas', '2015-12-22 23:39:01'),
(6, 1, 1, 0, 'adaapa', '2016-01-08 21:43:26');

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE IF NOT EXISTS `login` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_karyawan` int(11) DEFAULT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(32) DEFAULT NULL,
  `hak_akses` int(11) DEFAULT '1' COMMENT '1=menanganin komplain, 2=admin',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`id`, `id_karyawan`, `username`, `password`, `hak_akses`) VALUES
(1, 1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 2),
(2, 2, 'victor', 'e10adc3949ba59abbe56e057f20f883e', 3),
(3, 3, 'idvan', 'e10adc3949ba59abbe56e057f20f883e', 1),
(4, 11, 'fahmi', 'f11d50d63d3891a44c332e46d6d7d561', 4),
(5, 5, 'tina', 'ef2afe0ea76c76b6b4b1ee92864c4d5c', 1);

-- --------------------------------------------------------

--
-- Table structure for table `member`
--

CREATE TABLE IF NOT EXISTS `member` (
  `id_member` int(11) NOT NULL AUTO_INCREMENT,
  `kode_member` varchar(100) NOT NULL,
  `no_ktp_sim` varchar(20) NOT NULL,
  `nama` text NOT NULL,
  `lahir` varchar(50) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `ahli_waris` varchar(30) NOT NULL,
  `idagama` int(11) NOT NULL,
  `alamat` text NOT NULL,
  `no_tlp` varchar(30) NOT NULL,
  `idstatus` int(11) NOT NULL,
  `idgender` int(11) NOT NULL,
  `pendidikan` varchar(255) NOT NULL,
  `idpekerjaan` int(11) NOT NULL,
  `email` varchar(100) NOT NULL,
  PRIMARY KEY (`id_member`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `member`
--

INSERT INTO `member` (`id_member`, `kode_member`, `no_ktp_sim`, `nama`, `lahir`, `tanggal_lahir`, `ahli_waris`, `idagama`, `alamat`, `no_tlp`, `idstatus`, `idgender`, `pendidikan`, `idpekerjaan`, `email`) VALUES
(2, '0206888001', '1231232193812', 'Saepunajat', 'Sukabumi', '1986-02-17', 'santi', 1, 'Jl. Buah Batu Dalam 3 No.23 - Bandung', '0817170820', 1, 1, 'S1', 1, 'Saepunajat@gmail.com'),
(3, '0206888002', '32723267869845', 'Santi Susanti', 'Bandung', '1986-02-25', 'asep', 1, 'Jalan Kiara Condong No.24 Bandung', '09840589245', 1, 2, 'SMA', 1, 'santi.susanti185@yahoo.com'),
(4, '0206888003', '3214343242343', 'Vini Meilasari', 'Subang', '1978-08-12', 'Zidan', 1, 'JL.SURYA SUMANTRI NO.57\r\nBANDUNG', '081238768712', 2, 2, 'D3', 2, 'vini.meilasari08@yahoo.com'),
(5, '0206888004', '3123243344534', 'Dini Anggraeni', 'Jakarta', '1982-08-12', 'Aska Putra Maulana', 1, 'TAMAN KOPO INDAH II\r\nRT 11 / RW 02 DESA RAHAYU\r\nKEC MARGA ASIH\r\nBANDUNG', '08123187983', 2, 2, 'SMA', 1, 'dini_udin207@yahoo.com'),
(6, '0206888005', '3123124234234', 'Billy Elyasi', 'Bogor', '1988-12-07', 'Dimas', 1, 'KOM RUKO METRO MARGAHAYU\r\nRAYA KAV 17 - 20 SOEKARNO - HATTA\r\nBANDUNG', '0888716283', 1, 1, 'SMA', 1, 'billy.elyasi@gmail.com'),
(7, '0206888006', '231231676423', 'Dini Anggaraeni', 'bandung', '1982-10-12', 'ardi', 1, 'cibaduyut bandung', '08772342', 2, 2, 'D3', 3, 'dini.anggraeni@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `pekerjaan`
--

CREATE TABLE IF NOT EXISTS `pekerjaan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pekerjaan` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `pekerjaan`
--

INSERT INTO `pekerjaan` (`id`, `pekerjaan`) VALUES
(1, 'PELAJAR/MAHASISWA'),
(2, 'PNS'),
(3, 'Karyawan Swasta'),
(4, 'Ibu Rumah tangga');

-- --------------------------------------------------------

--
-- Table structure for table `penanggung_jawab`
--

CREATE TABLE IF NOT EXISTS `penanggung_jawab` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_jabatan` int(11) NOT NULL,
  `id_kategori_komplain` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `penanggung_jawab`
--

INSERT INTO `penanggung_jawab` (`id`, `id_jabatan`, `id_kategori_komplain`) VALUES
(1, 1, 28),
(2, 1, 29),
(3, 1, 31),
(4, 1, 32);

-- --------------------------------------------------------

--
-- Table structure for table `status`
--

CREATE TABLE IF NOT EXISTS `status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `statusPerkawinan` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `status`
--

INSERT INTO `status` (`id`, `statusPerkawinan`) VALUES
(1, 'SINGLE'),
(2, 'MENIKAH'),
(3, 'JANDA'),
(4, 'DUDA');

-- --------------------------------------------------------

--
-- Table structure for table `time_work`
--

CREATE TABLE IF NOT EXISTS `time_work` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_karyawan` int(11) DEFAULT NULL,
  `senin` enum('1','0') DEFAULT '0' COMMENT '0=Tidak, 1=Iya',
  `selasa` enum('1','0') DEFAULT '0' COMMENT '0=Tidak, 1=Iya',
  `rabu` enum('1','0') DEFAULT '0' COMMENT '0=Tidak, 1=Iya',
  `kamis` enum('1','0') DEFAULT '0' COMMENT '0=Tidak, 1=Iya',
  `jumat` enum('1','0') DEFAULT '0' COMMENT '0=Tidak, 1=Iya',
  `sabtu` enum('1','0') DEFAULT '0' COMMENT '0=Tidak, 1=Iya',
  `minggu` enum('1','0') DEFAULT '0' COMMENT '0=Tidak, 1=Iya',
  `jam_masuk` time DEFAULT NULL,
  `jam_keluar` time DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `time_work`
--

INSERT INTO `time_work` (`id`, `id_karyawan`, `senin`, `selasa`, `rabu`, `kamis`, `jumat`, `sabtu`, `minggu`, `jam_masuk`, `jam_keluar`) VALUES
(1, 1, '1', '1', '1', '1', '1', '1', '0', '07:00:00', '16:00:00'),
(2, 2, '1', '1', '1', '0', '1', '1', '1', '07:00:00', '16:00:00'),
(3, 3, '1', '1', '1', '1', '1', '1', '0', '07:00:00', '16:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `t_jabatan`
--

CREATE TABLE IF NOT EXISTS `t_jabatan` (
  `id_jabatan` int(11) NOT NULL AUTO_INCREMENT,
  `nama_jabatan` varchar(50) NOT NULL,
  PRIMARY KEY (`id_jabatan`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=20 ;

--
-- Dumping data for table `t_jabatan`
--

INSERT INTO `t_jabatan` (`id_jabatan`, `nama_jabatan`) VALUES
(1, 'Store Manager'),
(2, 'Spv. Food'),
(3, 'Spv. Nonfood'),
(4, 'Spv. Fresh'),
(5, 'Staf Buyer'),
(6, 'Spv. GMS'),
(7, 'Staf Personalia'),
(8, 'Staf Keuangan'),
(9, 'Staf IT'),
(10, 'Staf Receiving'),
(11, 'Staf CSO'),
(12, 'Kasir'),
(13, 'Service Crew'),
(14, 'Cleaning Service'),
(15, 'Adm. Buyer'),
(16, 'Keamanan'),
(17, 'Checker'),
(18, 'Gudang'),
(19, 'Adm. Receiving');

-- --------------------------------------------------------

--
-- Table structure for table `t_kategori_komplain`
--

CREATE TABLE IF NOT EXISTS `t_kategori_komplain` (
  `id_kategori` int(11) NOT NULL AUTO_INCREMENT,
  `nm_kategori` varchar(20) NOT NULL,
  PRIMARY KEY (`id_kategori`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=36 ;

--
-- Dumping data for table `t_kategori_komplain`
--

INSERT INTO `t_kategori_komplain` (`id_kategori`, `nm_kategori`) VALUES
(28, 'Kasir'),
(29, 'Harga'),
(30, 'SC (Service Crew)'),
(31, 'Pelayanan'),
(32, 'Pajangan Barang'),
(33, 'Stock Barang'),
(34, 'Kebersihan'),
(35, 'Lain - Lain');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
